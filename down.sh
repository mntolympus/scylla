#!/bin/bash
set -eux -p pipefail +h

# Use this script to quickly shutdown the Scylla webserver.
docker stop scylla
