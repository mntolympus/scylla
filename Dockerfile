FROM ubuntu:16.04

SHELL ["/bin/bash", "-c"]
ENV LANG=C.UTF-8

RUN set -eux; \
    apt-get update -y && apt-get install -y software-properties-common; \
    add-apt-repository -y ppa:ondrej/php; \
    apt-get update -y && apt-get upgrade -y; \
    apt-get install -y mariadb-client mariadb-server; \
    apt-get install -y php7.1 php7.1-mysql; \
    apt-get install -y apache2 apache2-mod-php7.1;

RUN set -eux; \
    debconf-set-selections <<< 'mysql-server mysql-server/root_password password fv14ODy7$Me6a6!Y'; \
    debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password fv14ODy7$Me6a6!Y';

COPY ui/build/ src/main/php/ /var/www/html/

RUN set -eux; \
    echo "ServerName localhost" >> /etc/apache2/apache2.conf; \
    service mysql start; \
    mysql -u root -p'fv14ODy7$Me6a6!Y' -e "GRANT ALL PRIVILEGES ON *.* TO 'cosctea3_hydra'@'%' IDENTIFIED BY '\$Test123';FLUSH PRIVILEGES;"; \
    mysql -u root -p'fv14ODy7$Me6a6!Y' -e "CREATE DATABASE IF NOT EXISTS cosctea3_hydra;"; \
    mysql -u root -p'fv14ODy7$Me6a6!Y' cosctea3_hydra < /var/www/html/protected/init/hydra.ddl; \
    mysql -u root -p'fv14ODy7$Me6a6!Y' cosctea3_hydra < /var/www/html/protected/init/hydra.dml;


CMD ["/bin/bash", "-c", "service mysql start && service apache2 start && tail -f /var/log/apache2/*.log"]
