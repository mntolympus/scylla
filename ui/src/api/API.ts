import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';

export interface APIResponse {
    status: number;
    message?: string;
}

export class API<T, P> {
    private config: AxiosRequestConfig;
    private creator: (props: P) => T;

    constructor(path: string, creator: (props: P) => T) {
        this.config = {
            url: 'api/' + path + '.php',
            baseURL: 'https://cosc4345-team4.com/',
            timeout: 10000,
            responseType: 'json',
        };
        this.creator = creator;
    }

    get(params?: {}): Promise<T[]> {
        var getConfig = this.config;
        getConfig.method = 'GET';
        getConfig.params = params;
        return new Promise<T[]>((resolve) => {
            axios(getConfig).then(((resp: AxiosResponse) => {
                const results: T[] = [];
                resp.data.data.map((res: P) => {
                    results.push(this.creator(res));
                });
                resolve(results);
            }),                   ((err: {}) => {

                console.log(err);
            }));
        });
    }

    getRaw(path: string): Promise<APIResponse> {
        var getConfig = this.config;
        getConfig.method = 'GET';
        getConfig.url = path;
        getConfig.responseType = 'text';
        return new Promise<APIResponse>((resolve) => {
            axios(getConfig).then(((resp: AxiosResponse) => {
                const response: APIResponse = {
                    status: resp.status,
                    message: resp.data
                };
                resolve(response);
            }),                   ((err: {}) => {

                console.log(err);
            }));
        });
    }

    create(body?: {}): Promise<APIResponse> {
        var createConfig = this.config;
        createConfig.method = 'POST';
        createConfig.data = body;
        return new Promise<APIResponse>((resolve) => {
            axios(createConfig).then(((resp: AxiosResponse) => {
                const response: APIResponse = {
                    status: resp.status,
                    message: resp.data
                };
                resolve(response);
            }),                      ((err: {}) => {
                console.log(err);
            }));
        });
    }

    update(body?: {}): Promise<APIResponse> {
        var createConfig = this.config;
        createConfig.method = 'PUT';
        createConfig.data = body;
        return new Promise<APIResponse>((resolve) => {
            axios(createConfig).then(((resp: AxiosResponse) => {
                const response: APIResponse = {
                    status: resp.status,
                    message: resp.data
                };
                resolve(response);
            }),                      ((err: {}) => {
                console.log(err);
            }));
        });
    }

    delete(body?: {}): Promise<APIResponse> {
        var getConfig = this.config;
        getConfig.method = 'DELETE';
        getConfig.data = body;
        return new Promise<APIResponse>((resolve) => {
            axios(getConfig).then(((resp: AxiosResponse) => {
                const response: APIResponse = {
                    status: resp.status,
                    message: resp.data
                };
                resolve(response);
            }),                   ((err: {}) => {
                console.log(err);
            }));
        });
    }

}
