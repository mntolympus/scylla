import { API } from 'api/API';
import { Sut, SutProps } from 'components/Sut';

export class SutAPI extends API<Sut, SutProps> {

    constructor() {
        super('sut', ((props: SutProps): Sut => {
            return new Sut(props);
        }));
    }

}