import { API } from 'api/API';
import { TestResult, TestResultProps } from 'components/TestResult';

export class ResultAPI extends API<TestResult, TestResultProps> {

    constructor() {
        super('execute', ((props: TestResultProps): TestResult => {
            return new TestResult(props);
        }));
    }

}