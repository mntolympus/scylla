import { API } from 'api/API';
import { Test, TestProps } from 'components/Test';

export class TestAPI extends API<Test, TestProps> {

    constructor() {
        super('test', ((props: TestProps): Test => {
            return new Test(props);
        }));
    }

}