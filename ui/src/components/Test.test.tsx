import * as React from 'react';
import * as Enzyme from 'enzyme';
const Adapter = require('enzyme-adapter-react-16');

import { Test, TestList } from 'components/Test';

Enzyme.configure({ adapter: new Adapter() });

it('Test renders without crashing', () => {
  expect(Enzyme.shallow(<Test testId={1}/>)).toMatchSnapshot();
});

it('Test renders test id', () => {
  const test = Enzyme.shallow(<Test testId={1} />);
  expect(test.find('.test').text()).toEqual(
    'ID: 1 -- Name:  -- Serial:  -- Threads:  -- Lifetime:  -- Script:  -- TypeId: '
  );
});

it('Test renders test name', () => {
  const test = Enzyme.shallow(<Test testId={1} testName="Testing" />);
  expect(test.find('.test').text()).toEqual(
    'ID: 1 -- Name: Testing -- Serial:  -- Threads:  -- Lifetime:  -- Script:  -- TypeId: '
  );
});

it('Test renders test is serial true', () => {
  const test = Enzyme.shallow(<Test testId={1} testIsSerial={true} />);
  expect(test.find('.test').text()).toEqual(
    'ID: 1 -- Name:  -- Serial: true -- Threads:  -- Lifetime:  -- Script:  -- TypeId: '
  );
});

it('Test renders test is serial false', () => {
  const test = Enzyme.shallow(<Test testId={1} testIsSerial={false} />);
  expect(test.find('.test').text()).toEqual(
    'ID: 1 -- Name:  -- Serial: false -- Threads:  -- Lifetime:  -- Script:  -- TypeId: '
  );
});

it('Test renders test num self parallel threads', () => {
  const test = Enzyme.shallow(<Test testId={1} testNumThreads={2} />);
  expect(test.find('.test').text()).toEqual(
    'ID: 1 -- Name:  -- Serial:  -- Threads: 2 -- Lifetime:  -- Script:  -- TypeId: '
  );
});

it('Test renders test test liftime limit', () => {
  const test = Enzyme.shallow(<Test testId={1} testLifetime={0} />);
  expect(test.find('.test').text()).toEqual(
    'ID: 1 -- Name:  -- Serial:  -- Threads:  -- Lifetime: 0 -- Script:  -- TypeId: '
  );
});

it('Test renders test script', () => {
  const test = Enzyme.shallow(<Test testId={1} testScript="test.py" />);
  expect(test.find('.test').text()).toEqual(
    'ID: 1 -- Name:  -- Serial:  -- Threads:  -- Lifetime:  -- Script: test.py -- TypeId: '
  );
});

it('Test renders all attributes', () => {
  const test = Enzyme.shallow(
    <Test 
      testId={1} 
      testName="Testing" 
      testIsSerial={true} 
      testNumThreads={2} 
      testLifetime={0} 
      testScript="test.py" 
    />
  );
  expect(test.find('.test').text()).toEqual(
    'ID: 1 -- Name: Testing -- Serial: true -- Threads: 2 -- Lifetime: 0 -- Script: test.py -- TypeId: 1'
  );
});

it('TestList renders without crashing', () => {
  expect(Enzyme.shallow((
      <TestList tests={[]} delete={((i: number) => () => { return; })} />)
    )
  ).toMatchSnapshot();
});