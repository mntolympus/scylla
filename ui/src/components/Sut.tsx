import * as React from 'react';

export interface SutProps {
    sutHostname: string;
    testSuiteId?: number;
}

export interface SutListProps {
    suts: Sut[];
    delete: (hostname: string) => () => void;    
}

export interface SutFormProps {
    sut: Sut;
    update: (s: Sut) => void;
}

export class Sut extends React.Component<SutProps> {
    constructor(props: SutProps) {
        super(props);
    }

    render() {
        return (
            <h3 className="sut">Hostname: {this.props.sutHostname} -- TestSuiteID: {this.props.testSuiteId}</h3>
        );
    }
}

export class SutList extends React.Component<SutListProps> {
    constructor(props: SutListProps) {
        super(props);
    }

    render() {
        return (
            <ul className="testList">
                {this.props.suts.map((sut: Sut) => {
                    return (
                        <li key={sut.props.testSuiteId}>
                            {sut.render()} 
                            <button><a href={'#/sut/' + sut.props.sutHostname}>Edit</a></button>
                            <button onClick={this.props.delete(sut.props.sutHostname)}>Delete</button>
                        </li>
                    );
                })}
            </ul>
        );
    }
}

export class SutForm extends React.Component<SutFormProps> {
    private cols = {
        'sutHostname': 'System Host Name',
        'testSuiteId':  'Test Suite Id',
    };

    constructor(props: SutFormProps) {
        super(props);
    }

    render() {
        return (
            <div className="form">
                {Object.keys(this.cols).map((prop: string, j: number) => {
                    return (
                        <div key={j}>
                            <label htmlFor={prop}>{this.cols[prop]}</label>
                            <input 
                                type="text" 
                                value={this.props.sut.props[prop]}
                                onChange={this.handleOnChange(prop)}
                            />
                        </div>
                    );
                })}
                <button onClick={this.handleOnSubmit()}>Submit</button>
            </div>
        );
    }

    handleOnChange(prop: string) {
        return (event: any): void => {         
            this.props.sut.props[prop] = event.target.value;
            this.forceUpdate();
            return event.target.value;
        };
    } 

    handleOnSubmit() {
        return (event: any): void => {
            this.props.update(this.props.sut);
        };
    }
}