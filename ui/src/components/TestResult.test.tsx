import * as React from 'react';
import * as Enzyme from 'enzyme';
const Adapter = require('enzyme-adapter-react-16');

import { TestResult, TestResultList } from 'components/TestResult';

Enzyme.configure({ adapter: new Adapter() });
const date = new Date().valueOf() / 1000;
const dateString = new Date().toLocaleString();

it('Test result renders without crashing', () => {
  expect(Enzyme.shallow(<TestResult executionId={1}/>)).toMatchSnapshot();
});

it('Test result renders execution id', () => {
  const testResult = Enzyme.shallow(<TestResult executionId={1} />);
  expect(testResult.find('.testResult').text()).toEqual(
    `SUT: -- Test Name:  -- Start Time:  -- End Time:  -- Status: `
  );
});

it('Test result renders start time', () => {
    const testResult = Enzyme.shallow(<TestResult executionId={1} executionTimeStart={date}/>);
    expect(testResult.find('.testResult').text()).toEqual(
        `SUT: -- Test Name:  -- Start Time: ${dateString} -- End Time:  -- Status: `
    );
});

it('Test result renders end time', () => {
    const testResult = Enzyme.shallow(<TestResult executionId={1} executionTimeEnd={date} />);
    expect(testResult.find('.testResult').text()).toEqual(
        `SUT: -- Test Name:  -- Start Time:  -- End Time: ${dateString} -- Status: `
    );
});

it('Test result renders status', () => {
    const testResult = Enzyme.shallow(<TestResult executionId={1} executionSuccess={true}/>);
    expect(testResult.find('.testResult').text()).toEqual(
        'SUT: -- Test Name:  -- Start Time:  -- End Time:  -- Status: SUCCESS'
    );
});

it('Test result renders test id', () => {
    const testResult = Enzyme.shallow(<TestResult executionId={1} testName="DirList" />);
    expect(testResult.find('.testResult').text()).toEqual(
        'SUT: -- Test Name: DirList -- Start Time:  -- End Time:  -- Status: '
    );
});

it('Test result renders test sut', () => {
    const testResult = Enzyme.shallow(<TestResult executionId={1} sutHostname="sut.hydra" />);
    expect(testResult.find('.testResult').text()).toEqual(
        'SUT: sut.hydra -- Test Name:  -- Start Time:  -- End Time:  -- Status: '
    );
});

it('Test renders all attributes', () => {
  const testResult = Enzyme.shallow(
    <TestResult 
      executionId={1} 
      executionTimeStart={date}
      executionTimeEnd={date}
      executionSuccess={true} 
      testName="DirList"
      sutHostname="sut.hydra" 
    />
  );
  expect(testResult.find('.testResult').text()).toEqual(
    `SUT: sut.hydra -- Test Name: DirList -- Start Time: ${dateString} -- End Time: ${dateString}` +
    ` -- Status: SUCCESS`
    );
});

it('TestList renders without crashing', () => {
    expect(Enzyme.shallow((
        <TestResultList 
            testResults={[]} 
            sort={((i: string) => () => { return; })}
            paginate={((i: number) => () => { return; })}
            modalIsOpen={true}
            modalContent={''}
            closeModal={() => { return; }}
            openModal={(i: number) => () => { return; }}
        />)
    )
  ).toMatchSnapshot();
});