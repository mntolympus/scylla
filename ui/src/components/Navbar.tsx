import * as React from 'react';
import { Link } from 'react-router-dom';

import 'style/components/navbar.scss';

export class NavBar extends React.Component {
    render() {
        return (
            <nav>
                <h3 className="nav-brand" >Scylla</h3>
                <Link to="/">Home</Link>
                <Link to="/results">Results</Link>
                <Link to="/tests">Tests</Link>
                <Link to="/test/add">Add Test</Link>
                <Link to="/suts">Suts</Link>
                <Link to="/sut/add">Add Sut</Link>
            </nav>
        );
    }
}
