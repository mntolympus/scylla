import * as React from 'react';

export interface TestProps {
    testId: number;
    testName?: string;
    testScript?: string;
    testIsSerial?: boolean;
    testNumThreads?: number;
    testLifetime?: number;
}

export interface TestListProps {
    tests: Test[];
    delete: (id: number) => () => void;
}

export interface TestFormProps {
    test: Test;
    update: (t: Test) => void;
}

export class Test extends React.Component<TestProps> {
    constructor(props: TestProps) {
        super(props);
    }

    render() {
        return (
            <h3 className="test">
                Name: {this.props.testName} -- 
                Script: {this.props.testScript} --
                Serial: {this.props.testIsSerial === true ? 'true' : 
                        (this.props.testIsSerial === false ? 'false' : '')} -- 
                Threads: {this.props.testNumThreads} -- 
                Lifetime: {this.props.testLifetime}
            </h3>
        );
    }
}

export class TestList extends React.Component<TestListProps> {
    constructor(props: TestListProps) {
        super(props);
    }

    render() {
        return (
            <ul className="testList">
                {this.props.tests.map((test: Test) => {
                    return (
                        <li className="object-inline" key={test.props.testId}>
                            {test.render()} 
                            <button><a href={'#/test/' + test.props.testId}>Edit</a></button>
                            <button onClick={this.props.delete(test.props.testId)}>Delete</button>
                        </li>
                    );
                })}
            </ul>
        );
    }
}

export class TestForm extends React.Component<TestFormProps> {
    private cols = {
        'testName': 'Test Name',
        'testScript':  'Script',
        'testIsSerial': 'Is Serial',
        'testNumThreads': 'Number of Threads',
        'testLifetime': 'Max Lifetime'
    };

    constructor(props: TestFormProps) {
        super(props);
    }

    render() {
        return (
            <div className="form">
                {Object.keys(this.cols).map((prop: string, j: number) => {
                    return (
                        <div key={j}>
                            <label htmlFor={prop}>{this.cols[prop]}</label>
                            <input 
                                type="text" 
                                value={this.props.test.props[prop]}
                                onChange={this.handleOnChange(prop)}
                            />
                        </div>
                    );
                })}
                <button onClick={this.handleOnSubmit()}>Submit</button>
            </div>
        );
    }

    handleOnChange(prop: string) {
        return (event: any): void => {
            this.props.test.props[prop] = event.target.value;
            this.forceUpdate();
            return event.target.value;
        };
    } 

    handleOnSubmit() {
        return (event: any): void => {
            const anySerialVal: any = this.props.test.props.testIsSerial;
            this.props.update(new Test({
                testId: this.props.test.props.testId,
                testLifetime: this.props.test.props.testLifetime,
                testName: this.props.test.props.testName,
                testNumThreads: this.props.test.props.testNumThreads,
                testScript: this.props.test.props.testScript,
                testIsSerial: (anySerialVal === 'true' ? true : false)
            }));
        };
    }
}