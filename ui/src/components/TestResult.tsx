import * as React from 'react';
import * as Modal from 'react-modal';

const timeOf = (time?: number) => {
    // multiple by 1000 because php return seconds, and typescript wants miliseconds
    return (time !== undefined && time !== 0) ? new Date(time * 1000).toLocaleString() : '';
};

const status = (success?: boolean, endTime?: number) => {
    return (endTime === undefined || endTime === 0) ? 'IN PROGRESS' : (success === true ? 'SUCCESS' : 'FAIL');
};

const customStyles = {
    content : {
      top                   : '50%',
      left                  : '50%',
      right                 : 'auto',
      bottom                : 'auto',
      marginRight           : '-50%',
      transform             : 'translate(-50%, -50%)'
    }
  };

export interface TestResultProps {
    executionId: number;
    executionTimeStart?: number;
    executionTimeEnd?: number;
    executionSuccess?: boolean;
    testName?: string;
    sutHostname?: string;
    output?: string;
}

export interface TestResultListProps {
    testResults: TestResult[];
    sort: (col: string) => () => void;
    sortCol?: string;
    paginate: (amount: number) => () => void;
    page?: number;
    modalIsOpen: boolean;
    modalContent?: string;
    openModal: (execId: number) => () => void;
    closeModal: () => void;
}

export class TestResult extends React.Component<TestResultProps> {
    constructor(props: TestResultProps) {
        super(props);
    }

    render() {
        return (
            <h3 className="testResult">
                SUT: {this.props.sutHostname} -- 
                Test Name: {this.props.testName} -- 
                Start Time: {timeOf(this.props.executionTimeStart)} -- 
                End Time: {timeOf(this.props.executionTimeEnd)} -- 
                Status: {status(this.props.executionSuccess)}
            </h3>
        );
    }
}

export class TestResultList extends React.Component<TestResultListProps> {
    constructor(props: TestResultListProps) {
        super(props);
    }

    jsonToTsx(prop: string, testResult: TestResult): string {
        const EXEC_TIME_END = 'executionTimeEnd';
        var val = '';
        switch (prop) {
            case 'executionTimeStart':
                val = timeOf(testResult.props[prop]);
                break;
            case 'executionTimeEnd':
                val = timeOf(testResult.props[prop]);
                break;
            case 'executionSuccess':
                val = status(testResult.props[prop], testResult.props[EXEC_TIME_END]);
                break;
            default:
                val = testResult.props[prop];          
        }
        return val;
    }
    
    render() {
        const cols = {
            'sutHostname': 'SUT',
            'testName': 'Test Name',     
            'executionSuccess': 'Status',
            'executionTimeStart': 'Start Time',
            'executionTimeEnd': 'End Time'
        };
        return (
            <div className="test-result-table data-table">
                {/* Header */}
                <div className="data-table-header">
                    {Object.keys(cols).map((prop: string, i: number) => {
                        return (
                            <div className="data-table-cell" key={i} onClick={this.props.sort(prop)}>
                                {cols[prop]}  <i className={'arrow' + (this.props.sortCol === prop ? ' active' : '')}/>
                            </div>
                        );
                    })}
                    <div className="data-table-cell">Duration</div>
                </div>
                {/* Result Rows */}
                {this.props.testResults.map((testResult: TestResult, i: number) => {
                    return (
                        <div key={i} className="data-table-row">{
                            Object.keys(cols).map((prop: string, j: number) => {
                                return (
                                    <div className="data-table-cell" key={`${i}${j}`}>
                                        {this.jsonToTsx(prop, testResult)}
                                    </div>
                                );
                            })}
                            <div className="data-table-cell" key={`${i}-duration`}>
                            {(testResult.props.executionTimeEnd === undefined 
                                || testResult.props.executionTimeEnd === 0) 
                            ? '' : 
                            ((testResult.props.executionTimeEnd  || 0) - (testResult.props.executionTimeStart || 0)) 
                            + ' seconds'}
                            </div>
                            <div className="data-table-cell" key={`${i}-log`}>
                                <button onClick={this.props.openModal(testResult.props.executionId)}>View Logs</button>
                            </div>

                        </div>
                    );
                })}
                {/* Pagination */}
                <div key={`paginate`} className="data-table-pagination">
                    <button onClick={this.props.paginate(-1)}>&larr;</button>
                    <button onClick={this.props.paginate(1)}>&rarr;</button>
                    <span>  Page: {this.props.page || 0}</span>
                </div>
                <Modal
                    isOpen={this.props.modalIsOpen}
                    onRequestClose={this.props.closeModal}
                    style={customStyles}
                    contentLabel="Execution Logs"
                >
                    <div>{this.props.modalContent}</div>
                    <div />
                    <button onClick={this.props.closeModal}>close</button>
                    
                </Modal>
            </div>
        );
    }
}