import * as React from 'react';
import * as Enzyme from 'enzyme';
const Adapter = require('enzyme-adapter-react-16');

import { Sut, SutList } from 'components/Sut';

Enzyme.configure({ adapter: new Adapter() });

it('Sut renders without crashing', () => {
  expect(Enzyme.shallow(<Sut sutHostname="test.lab" />)).toMatchSnapshot();
});

it('Sut renders hostname', () => {
  const sut = Enzyme.shallow(<Sut sutHostname="test.lab" />);
  expect(sut.find('.sut').text()).toEqual('Hostname: test.lab -- TestSuiteID: ');
});

it('Sut renders all attributes', () => {
    const sut = Enzyme.shallow(<Sut sutHostname="test.lab" testSuiteId={1} />);
    expect(sut.find('.sut').text()).toEqual('Hostname: test.lab -- TestSuiteID: 1');
});

it('SutList renders without crashing', () => {
  expect(Enzyme.shallow((
      <SutList suts={[]} delete={((hostname: string) => () => { return; })}/>)
    )
  ).toMatchSnapshot();
});