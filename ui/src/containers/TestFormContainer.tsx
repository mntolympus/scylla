import * as React from 'react';
import { APIResponse } from 'api/API';
import { TestAPI } from 'api/TestAPI';
import { Test, TestForm } from 'components/Test';

import 'style/containers/content.scss';

export interface TestFormState {
    test: Test;
}

export interface TestFormProps {
    match?: any;
}

export class TestFormContainer extends React.Component<TestFormProps, TestFormState> {
    constructor(props: TestFormProps) {
        super(props);
        this.state = {
            test: new Test({testId: -1})
        };
    }

    update(test: Test) {
        const testAPI = new TestAPI();
        if (test.props.testId === -1) {
            testAPI.create(test.props).then(((resp: APIResponse) => {
            }));
        } else {
            testAPI.update(test.props).then(((resp: APIResponse) => {
            }));
        }
    }

    async componentDidMount() {
        var testId = -1;
        if (this.props.match !== undefined && this.props.match.params !== undefined) {
            testId = Number(this.props.match.params.testId) || -1;
        }
        if (testId !== -1) {
            var testAPI = new TestAPI();
            var tests = await testAPI.get({'testId': this.props.match.params.testId});
            if (tests.length > 0) {
                this.setState({
                    test: tests[0]
                });
            }
        }
    }

    render() {
        return (
            <div className="content">
                <TestForm test={this.state.test} update={this.update}/>
            </div>
        );
    }
}