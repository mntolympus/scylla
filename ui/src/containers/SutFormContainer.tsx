import * as React from 'react';
import { APIResponse } from 'api/API';
import { SutAPI } from 'api/SutAPI';
import { Sut, SutForm } from 'components/Sut';

import 'style/containers/content.scss';

export interface SutFormState {
    sut: Sut;
}

export interface SutFormProps {
    match?: any;
}

export class SutFormContainer extends React.Component<SutFormProps, SutFormState> {
    constructor(props: SutFormProps) {
        super(props);
        this.state = {
            sut: new Sut({sutHostname: ''})
        };
    }

    update = (stateHostname: string) => (sut: Sut) => {
        const sutAPI = new SutAPI();
        // create sut if sutHostname wasn't populated by path params
        if (stateHostname === '') {
            sutAPI.create(sut.props).then(((resp: APIResponse) => {
            }));
        } else {
            sutAPI.update(sut.props).then(((resp: APIResponse) => {
            }));
        }
    }

    async componentDidMount() {
        var sutHostname = '';
        if (this.props.match !== undefined && this.props.match.params !== undefined) {
            sutHostname = this.props.match.params.sutHostname || '';
        }
        if (sutHostname !== '' && sutHostname !== 'add') {
            var sutAPI = new SutAPI();
            var suts = await sutAPI.get({'sutHostname': sutHostname});
            if (suts.length > 0) {
                this.setState({
                    sut: suts[0]
                });
            }
        }
    }

    render() {
        return (
            <div className="content">
                <SutForm sut={this.state.sut} update={this.update(this.state.sut.props.sutHostname)}/>
            </div>
        );
    }
}