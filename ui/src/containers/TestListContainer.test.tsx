import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { TestListContainer } from 'containers/TestListContainer';

it('TestListContainer renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<TestListContainer />, div);
});
