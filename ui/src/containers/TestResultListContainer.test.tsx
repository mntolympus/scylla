import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { TestResultListContainer } from 'containers/TestResultListContainer';

it('TestResultListContainer renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<TestResultListContainer />, div);
});
