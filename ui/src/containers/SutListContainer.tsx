import * as React from 'react';
import { SutAPI } from 'api/SutAPI';
import { APIResponse } from 'api/API';
import { Sut, SutList } from 'components/Sut';

import 'style/containers/content.scss';

export interface SutListState {
    suts: Sut[];
}

export class SutListContainer extends React.Component<{}, SutListState> {
    constructor() {
        super();
        this.state = {
            suts: []
        };
    }

    delete = (sutHostname: string) => () => {
        var sutAPI = new SutAPI();
        
        sutAPI.delete({'sutHostname': sutHostname}).then(((resp: APIResponse) => {
        }));   
    }

    async componentDidMount() {
        var sutAPI = new SutAPI();
        this.setState({suts: await sutAPI.get()});
    }

    render() {
        return (
            <div className="content">
                <SutList suts={this.state.suts} delete={this.delete}/>
            </div>
        );
    }
}