import * as React from 'react';
import { TestAPI } from 'api/TestAPI';
import { APIResponse } from 'api/API';
import { Test, TestList } from 'components/Test';

import 'style/containers/content.scss';

export interface TestListState {
    tests: Test[];
}

export class TestListContainer extends React.Component<{}, TestListState> {
    constructor() {
        super();
        this.state = {
            tests: []
        };
    }

    delete = (testId: number) => () => {
        var testAPI = new TestAPI();
        
        testAPI.delete({'testId': testId}).then(((resp: APIResponse) => {
            this.forceUpdate();
        }));   
    }

    async componentDidMount() {
        var testAPI = new TestAPI();
        this.setState({tests: await testAPI.get()});
    }

    render() {
        return (
            <div className="content">
                <TestList tests={this.state.tests} delete={this.delete} />
            </div>
        );
    }
}