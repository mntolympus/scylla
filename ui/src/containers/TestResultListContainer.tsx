import * as React from 'react';
import { ResultAPI } from 'api/ResultAPI';
import { APIResponse } from 'api/API';
import { TestResult, TestResultList } from 'components/TestResult';

import 'style/containers/content.scss';
import 'style/containers/test-result.scss';

export interface TestResultListState {
    testResults: TestResult[];
    sortCol?: string;
    currentPage: number;
    modalIsOpen: boolean;
    modalContent?: string;
}

export class TestResultListContainer extends React.Component<{}, TestResultListState> {
    constructor() {
        super();
        this.state = {
            testResults: [],
            sortCol: '',
            currentPage: 0,
            modalIsOpen: false
        };
        this.openModal = this.openModal.bind(this);
        this.closeModal = this.closeModal.bind(this);   
    }

    openModal = (execId: number) => () => {
        var resultAPI = new ResultAPI();
        const path = 'executionLogs/' + execId + '.log';        
        resultAPI.getRaw(path).then(((resp: APIResponse) => {
            this.setState({
                modalIsOpen: true,
                modalContent: resp.message
            });
            this.forceUpdate();
        }));  
    }

    closeModal = () => {
        this.setState({
            modalIsOpen: false,
            modalContent: ''
        });
        this.forceUpdate();
        
    }
    
    sort = (col: string) => () => {
        if (col === this.state.sortCol) {
            this.setState({sortCol: ''});
        } else {
            this.setState({sortCol: col});            
            this.state.testResults.sort((a: TestResult, b: TestResult): number => {
                if (typeof a.props[col] === 'string') {
                    return b.props[col].localeCompare(a.props[col]);
                } else {
                    return b.props[col] - a.props[col];
                }
            });
        } 
        this.forceUpdate();
    }

    paginate = (amount: number) => () => {
        if (this.state.currentPage + amount >= 0 
            && this.state.currentPage + amount * 10 < this.state.testResults.length) {
            this.setState({currentPage: this.state.currentPage + amount});
            this.forceUpdate();
        }

    }

    async componentDidMount() {
        var testResultAPI = new ResultAPI();
        this.setState({testResults: await testResultAPI.get()});
    }

    render() {
        return (
            <div className="content">
                <TestResultList 
                    testResults={this.state.testResults.slice(this.state.currentPage * 10, 
                                                              this.state.currentPage * 10 + 10)} 
                    sort={this.sort} 
                    sortCol={this.state.sortCol}
                    paginate={this.paginate}
                    page={this.state.currentPage}
                    modalIsOpen={this.state.modalIsOpen}
                    modalContent={this.state.modalContent}
                    closeModal={this.closeModal}
                    openModal={this.openModal}
                />
            </div>
        );
    }
}