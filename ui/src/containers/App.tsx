import * as React from 'react';
import { NavBar } from 'components/Navbar';
import { Route, Switch } from 'react-router-dom';

import { TestListContainer } from 'containers/TestListContainer';
import { TestFormContainer } from 'containers/TestFormContainer';
import { SutListContainer } from 'containers/SutListContainer';
import { SutFormContainer } from 'containers/SutFormContainer';
import { TestResultListContainer } from 'containers/TestResultListContainer';

import 'style/containers/app.scss';

export class App extends React.Component {
  render() {
    return (
      <div className="app">
        <NavBar />
        <Switch>
          <Route path="/test/:testId" component={TestFormContainer} />
          <Route path="/tests" component={TestListContainer} />
          <Route path="/sut/:sutHostname" component={SutFormContainer} />
          <Route path="/suts" component={SutListContainer} />
          <Route path="/results" component={TestResultListContainer} />
        </Switch>
      </div>
    );
  }
}