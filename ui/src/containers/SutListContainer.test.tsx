import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { SutListContainer } from 'containers/SutListContainer';

it('SutListContainer renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<SutListContainer />, div);
});
