#!/bin/bash
set -eu -p pipefail +h

# Use this script to build and run a LAMP webserver for Scylla UI and APIs.
echo "building..."
echo " [=]building ui"; \
    (cd ui && npm run build > /dev/null && cd ../) \
    || (ehco "ui build failed." && exit 1); 

echo " [=]building dev container"; \
    docker build -t scylla . > /dev/null \
    || (ehco "docker build failed." && exit 1);

echo "press ctrl+c to ext..."
echo " [=]running dev container"; \
    docker run --rm -it --name scylla -v $(pwd)/src/main/php/api/:/var/www/html/api/ -p 80:80 scylla;
