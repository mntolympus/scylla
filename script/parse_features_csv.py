#!/usr/local/bin/python3
# this file parses a trello json export into a more readable csv format

import csv
import json
import os

DIR_PATH = os.path.dirname(os.path.realpath(__file__))
FEATURE_FILE = DIR_PATH+"/features.json"
CSV_OUT = csv.writer(open(DIR_PATH+"/features_out.csv", 'w'))
CSV_OUT.writerow(["Name","Description","Priority","Owner","Sprint"])

def clean(stripString):
    if len(stripString)==0:
        return "n/a"
    for char in ['\"', "\n"]:
        stripString=stripString.replace(char, "")
    return stripString

def printfeature(featureFile):
    with open(featureFile) as featuresRaw:
        features = json.load(featuresRaw)

    #print all card names
    for card in features['cards']:
        name = card['name']
        priority = str.join(" ", list(label['name'] for label in card['labels'] if "Priority" in label['name']))
        sprint = str.join(" ", list(label['name'] for label in card['labels'] if "Sprint" in label['name']))
        owners = str.join(" and ", list(member['fullName'] for member in features['members'] for memberId in card['idMembers'] if member['id']==memberId))
        owners = owners.replace("Q", "TJ")
        owners = owners.replace("TheThrasherr", "Stephanie")
        desc = card['desc']
        CSV_OUT.writerow([clean(name),clean(desc),clean(priority),clean(owners),clean(sprint)])

printfeature(FEATURE_FILE)
