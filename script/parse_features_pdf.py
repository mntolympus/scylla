# #!/usr/local/bin/python3
# this file parses a trello json export into a readable pdf format

import datetime
import json
import os
import subprocess
import time

from mailer import mailFile
from reportlab.lib.pagesizes import letter
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import inch
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer

time = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d')
outputFilename = "backlog_{}.pdf".format(time)
DOC = SimpleDocTemplate(
    outputFilename,
    rightMargin=inch / 2,
    leftMargin=inch / 2,
    topMargin=inch / 2,
    bottomMargin=inch / 2,
    pagesize=letter,
)

DIR_PATH = os.path.dirname(os.path.realpath(__file__))
FEATURE_FILE = DIR_PATH + "/features.json"
STYLES = getSampleStyleSheet()
SPACE = 7
HEAD = [Paragraph('COSC Team 4', STYLES['BodyText']), Spacer(1, SPACE),
        Paragraph('Backlog ' + str(datetime.date.today()), STYLES['BodyText']), Spacer(1, SPACE),
        Paragraph('Jason Morris, Stephanie Thrash', STYLES['BodyText']),
        Paragraph('Hannah Smith, TJ Challstrom', STYLES['BodyText']), Spacer(1, 2 * SPACE)]


def clean(stripString, dash=True):
    if len(stripString) == 0:
        return ""
    for char in ['\"', "\n"]:
        stripString = stripString.replace(char, "")
    return " -- " + stripString if dash else stripString


def printfeature(featureFile):
    backlog = [Paragraph('Section III Backlog', STYLES['Heading3'])]
    inProgress = [Paragraph('Section II - Work In Progress', STYLES['Heading3'])]
    done = [Paragraph('Section I - Complete Features', STYLES['Heading3'])]

    with open(featureFile) as featuresRaw:
        features = json.load(featuresRaw)

    # print all card names
    for card in features['cards']:
        if not card['closed']:
            name = card['name']
            priority = str.join(" ", list(label['name'] for label in card['labels'] if "Priority" in label['name']))
            if "Asg" in name[:5] and len(priority) == 0: continue
            sprint = str.join(" ", list(label['name'] for label in card['labels'] if "Sprint" in label['name']))
            owners = str.join(" and ", list(
                member['fullName'] for member in features['members'] for memberId in card['idMembers'] if
                member['id'] == memberId))
            owners = owners.replace("Q", "TJ")
            owners = owners.replace("TheThrasherr", "Stephanie")
            desc = card['desc']
            status = card['idList']
            if status == "59bf24bf4c609a1cef1ad4a9":
                date = 'Completed on: {}'.format(str(card['dateLastActivity']).split('T')[0])
                output = '{} {} {} {} {} {}'.format(clean(name, False), clean(owners), clean(priority), clean(sprint),
                                             clean(desc), clean(date))
            else:
                output = '{} {} {} {} {}'.format(clean(name, False), clean(owners), clean(priority), clean(sprint),
                                             clean(desc))

            if status == "59bf24bf4c609a1cef1ad4a9":
                done.append(Paragraph(output, STYLES['Normal']))
                done.append(Spacer(1, SPACE))
            elif status == "59bf24ac6d85fd589e645975":
                inProgress.append(Paragraph(output, STYLES['Normal']))
                inProgress.append(Spacer(1, SPACE))
            elif status == "59c543c988c9980623129626":
                backlog.append(Paragraph(output, STYLES['Normal']))
                backlog.append(Spacer(1, SPACE))
            else:
                print("List not found for card", name)

    paragraphs = list(pi for px in [HEAD, done, inProgress, backlog] for pi in px)
    DOC.build(paragraphs)


printfeature(FEATURE_FILE)
try:
    retCode = -10
    if os.name == "nt":
        retCode = subprocess.call("start " + outputFilename, shell=True)
    else:
        retCode = subprocess.call("open " + outputFilename, shell=True)
    if retCode < 0:
        print("Child was terminated by signal - {}".format(retCode))
    else:
        print("Child returned - {}".format(retCode))
except OSError as e:
    print("Execution failed:", e)
print("Please review {}.".format(outputFilename))
choice = input("Would you like to mail this file to the COSC4345 mailing list(y/n)? ")
if choice is "y" or choice is "Y":
    mailFile(outputFilename)
else:
    print("Not mailing file.")
print("Completed.")
