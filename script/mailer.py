import requests


def mailFile(fileName):
    url = 'https://tchallst.create.stedwards.edu/mailer.php'
    key = 'na@9QjD5h8mbsDzpH7H8mz$%$A+zc4Uz'
    params = {'key': key, 'filename': fileName}
    print('Attempting file upload of ' + fileName)
    with open(fileName, 'rb') as file:
        files = {fileName: file}

        r = requests.post(url, params=params, files=files)
        print(r.status_code)
        print(r.text)
