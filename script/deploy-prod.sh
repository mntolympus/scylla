#!/bin/bash
set -euo pipefail
IFS=$'\n\t'

cd ../
read -p "Run this from the script dir of the scylla repo. Continue? (y/n): " -n 1 -r
echo
if [[ ! $REPLY =~ ^[Yy]$ ]]
then
    [[ "$0" = "$BASH_SOURCE" ]] && exit 1 || return 1 # handle exits from shell or function but don't exit interactive shell
fi

echo "== building ui"
(cd ui && npm install && npm run build &>/dev/null && cd ../) || (echo "ui build failed" && exit 1)
echo "== uploading ui"
(scp -i ~/.ssh/cosc-4345.rsa -r ui/build/* cosctea3@cosc4345-team4.com:/home/cosctea3/www/ &>/dev/null) || (echo "ui deploy failed" && exit 1)
echo "== uploading php"
(scp -i ~/.ssh/cosc-4345.rsa -r src/main/php/* cosctea3@cosc4345-team4.com:/home/cosctea3/www/ &>/dev/null) || (echo "php deploy failed" && exit 1)
echo "== uploading python"
(scp -i ~/.ssh/cosc-4345.rsa -r src/main/python/* cosctea3@cosc4345-team4.com:/home/cosctea3/www/test/ &> /dev/null) || (echo "python deploy failed" && exit 1)
