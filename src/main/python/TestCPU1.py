#
# TestCPU1.py
#
# Simple program that tests the CPU
#

import json
import os
import shutil
import sys
import time
import traceback
import random

def main():
    '''
    Runs the test case and outputs JSON message to stdout.
    '''
    print((runTest()))
    
def runTest():
    '''
    Runs the test case and handles exceptions.
        '''
    if sys.platform == "win32":
        # On Windows, the best timer is time.clock()
        default_timer = time.clock
    else:
        # On most other platforms the best timer is time.time()
        default_timer = time.time
        
    result = {'TestStatus': 'ERROR', 'Messages': []}
    try:
        start_time = default_timer()
        result = run()
        end_time = default_timer()
        result['Messages'].append('Completed test in %f seconds' % (end_time - start_time))
    except Exception as ex:
        result['TestStatus'] = 'ERROR'
        result['Messages'].append(traceback.format_exc())
    except:
        result['TestStatus'] = 'ERROR'
        result['Messages'].append('Unknown error')
    return json.dumps(result)

    
def run():
    '''
    Tests CPU
    '''
    result = {'TestStatus': 'ERROR', 'Messages': []}
    
    # Create array with random numbers
    LENGTH = 1000000
    values = list(range(0, LENGTH))
    for i in range(0, LENGTH):
        values[i] = random.randint(1,LENGTH)

    # Verify array length
    if len(values) != LENGTH:          
        result['Messages'].append('Error: wrong array length: %d' % len(values))
        return result
    
    # Verify array
      
    for i in range(0, LENGTH):
        if values[i] < 1 or values[i] > LENGTH:
            result['Messages'].append('Error: CPU failed: values[%d] = %d' % (i, values[i]))
            return result                                                    
    
    result['TestStatus'] = 'SUCCESS'
    return result


if __name__ == '__main__':
    main()
