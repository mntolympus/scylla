#
# TestIntegerMath1.py
#
# Simple program that tests integer math
#

import json
import os
import time
import traceback


def main():
    """
    Runs the test case and outputs JSON message to stdout.
    """
    print(runTest())


def runTest():
    """
    Runs the test case and handles exceptions.
        """
    if os.name is "nt":
        # On Windows, the best timer is time.clock()
        default_timer = time.clock
    else:
        # On most other platforms the best timer is time.time()
        default_timer = time.time

    result = {'TestStatus': 'ERROR', 'Messages': []}
    try:
        start_time = default_timer()
        result = run()
        end_time = default_timer()
        result['Messages'].append('Completed test in %f seconds' % (end_time - start_time))
    except Exception as ex:
        result['TestStatus'] = 'ERROR'
        result['Messages'].append(traceback.format_exc())
    except:
        result['TestStatus'] = 'ERROR'
        result['Messages'].append('Unknown error')
    return json.dumps(result)


def run():
    """
    Tests integer math
    """
    result = {'TestStatus': 'ERROR', 'Messages': []}

    count = 1000
    for i in range(count):
        res = 5 + 5
        if res != 10:
            result['Messages'].append('Addition failed')
            return result
    
        res = 6 - 4
        if res != 2:
            result['Messages'].append('Subtraction failed')
            return result
    
        res = 8 * 2
        if res != 16:
            result['Messages'].append('Multiplication failed')
            return result
    
        res = 20 / 5
        if res != 4:
            result['Messages'].append('Division failed')
            return result
    
        res = 20 % 6
        if res != 2:
            result['Messages'].append('Mod failed')
            return result

    result['TestStatus'] = 'SUCCESS'
    return result


if __name__ == '__main__':
    main()
