import unittest
import json
import TestIntegerMath1

class TestPythonTests(unittest.TestCase):

    def testTestIntegerMath1_success(self):
        '''
        '''
        result = json.loads(TestIntegerMath1.runTest())
        self.assertEqual(len(result), 2)
        self.assertTrue('TestStatus' in result)
        self.assertTrue('Messages' in result)
        self.assertTrue(result['TestStatus'])
        self.assertTrue(len(result['Messages']) == 1)


if __name__ == '__main__':
    unittest.main()
