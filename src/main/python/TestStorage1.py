    
#
# TestStorage1.py
#
# Simple program that tests file system access.
#

import json
import os
import shutil
import sys
import time
import traceback
import uuid

def main():
    '''
    Runs the test case and outputs JSON message to stdout.
    '''
    print((runTest()))    

def runTest():
    '''
    Runs the test case and handles exceptions.
        '''
    if sys.platform == "win32":
        # On Windows, the best timer is time.clock()
        default_timer = time.clock
    else:
        # On most other platforms the best timer is time.time()
        default_timer = time.time
        
    result = {'TestStatus': 'ERROR', 'Messages': []}
    try:
        start_time = default_timer()
        result = run()
        end_time = default_timer()
        result['Messages'].append('Completed test in %f seconds' % (end_time - start_time))
    except Exception as ex:
        result['TestStatus'] = 'ERROR'
        result['Messages'].append(traceback.format_exc())
    except:
        result['TestStatus'] = 'ERROR'
        result['Messages'].append('Unknown error')
    return json.dumps(result)

    
def run():
    '''
    Tests creating, writing, and reading of files.
    '''
    result = {'TestStatus': 'ERROR', 'Messages': []}

    NUM_FILES = 1000
    
    # Create Test Directory
    test_dir = 'test_storage_%s' % str(uuid.uuid4())
    if os.path.exists(test_dir):
        result['Messages'].append('Error: %s directory already exists and will be overwritten')
        shutil.rmtree(test_dir)
    os.mkdir(test_dir)

    # Create files    
    for i in range(1, NUM_FILES + 1):
        filename = '%s%s' % ('file', i)
        with open(os.path.join(test_dir, filename), 'w') as f:
            f.write('hello world')
        
    # Test that files exist
    files = os.listdir(test_dir)
    if len(files) != NUM_FILES:
        result['Messages'].append('Error: number of files is incorrect')
        return result
    for i in range(1, NUM_FILES + 1):
        filename = '%s%s' % ('file', i)
        with open(os.path.join(test_dir, filename), 'r') as f:
            contents = f.read()
            if contents != 'hello world':
                result['Messages'].append('Error: file %s did not have expected contents' % (filename))
                return result
    
    # Cleanup    
    shutil.rmtree(test_dir)
    
    result['TestStatus'] = 'SUCCESS'
    return result


if __name__ == '__main__':
    main()
