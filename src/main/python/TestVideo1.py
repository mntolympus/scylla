#
#TestVideo1.py
#
#Simple program that tests the video system
#

import json
import os
import shutil
import sys
import time
import traceback
import pyscreenshot as ImageGrab
import uuid

def main():
    '''
    Run the test case and outputs JSON message to stdout.
    '''
    print((runTest()))

def runTest():
    '''
    Runs the test case and handles exceptions.
    '''
    if sys.platform == "win32":
        default_timer = time.clock
    else:
        default_timer = time.time

    result = {'TestStatus': 'ERROR', 'Messages': []}
    try:
        start_time = default_timer()
        result = run()
        end_time = default_timer()
        result['Messages'].append('Completed test in %f seconds' % (end_time - start_time))
    except Exception as ex:
        result['TestStatus'] = 'ERROR'
        result['Messages'].append(traceback.format_exc())
    except:
        result['TestStatus'] = 'ERROR'
        result['Message'].append('Unknown error')
    return json.dumps(result)

def run():
    '''
    Tests creating screenshot files using pyscreenshot.
    '''
    result = {'TestStatus': 'ERROR', 'Messages': []}

    NUM_FILES = 10

    # Create test directory
    test_dir = 'test_video_%s' % str(uuid.uuid4())
    if os.path.exists(test_dir):
        result['Messages'].append('Warning: %s directory already exists and will be overwritten')
        shutil.rmtree(test_dir)
    os.mkdir(test_dir)

    # Create files
    for i in range(1, NUM_FILES + 1):
        filename = '%s%s.png' % ('file', i)
        with open(os.path.join(test_dir, filename), 'w') as f:
            f = ImageGrab.grab()
            f.save(os.path.join(test_dir, filename))

    # Test that files exist
    files = os.listdir(test_dir)
    if len(files) != NUM_FILES:
        result['Messages'].append('Error: number of files is incorrect')
        return result
    for i in range(1, NUM_FILES + 1):
        filename = '%s%s.png' % ('file', 1)
        with open(os.path.join(test_dir, filename), 'r') as f:
            statinfo = os.stat(os.path.join(test_dir, filename))
            size = statinfo.st_size
            if size <= 0:
                result['Messages'].append('Error: screenshot %s did not save correctly, size is %d' % (filename, size))
                return result

    # Cleanup
    shutil.rmtree(test_dir)

    result['TestStatus'] = 'SUCCESS'
    return result

if __name__ == '__main__':
    main()
