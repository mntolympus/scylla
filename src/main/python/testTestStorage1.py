import unittest
import json
import TestStorage1

class TestPythonTests(unittest.TestCase):

    def testTestStorage1_success(self):
        '''
        '''
        result = json.loads(TestStorage1.runTest())
        self.assertEqual(len(result), 2)
        self.assertTrue('TestStatus' in result)
        self.assertTrue('Messages' in result)
        self.assertTrue(result['TestStatus'])
        self.assertTrue((len(result['Messages']) == 1 or 
                         len(result['Messages']) == 2))



if __name__ == '__main__':
    unittest.main()
