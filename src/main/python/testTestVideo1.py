import unittest
import json
import TestVideo1

class TestPythonTests(unittest.TestCase):

    def testTestVideo1_success(self):
        '''
        '''
        result = json.loads(TestVideo1.runTest())
        self.assertEqual(len(result), 2)
        self.assertTrue('TestStatus' in result)
        self.assertTrue('Messages' in result)
        self.assertTrue(result['TestStatus'])
        self.assertTrue((len(result['Messages']) == 1 or 
                         len(result['Messages']) == 2))
        
    

if __name__ == '__main__':
    unittest.main()
