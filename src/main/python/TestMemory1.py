#
# TestMemory1.py
#
# Simple program that tests memory
#

import json
import os
import shutil
import sys
import time
import traceback

def main():
    '''
    Runs the test case and outputs JSON message to stdout.
    '''
    print((runTest()))
    

def runTest():
    '''
    Runs the test case and handles exceptions.
        '''
    if sys.platform == "win32":
        # On Windows, the best timer is time.clock()
        default_timer = time.clock
    else:
        # On most other platforms the best timer is time.time()
        default_timer = time.time
        
    result = {'TestStatus': 'ERROR', 'Messages': []}
    try:
        start_time = default_timer()
        result = run()
        end_time = default_timer()
        result['Messages'].append('Completed test in %f seconds' % (end_time - start_time))
    except Exception as ex:
        result['TestStatus'] = 'ERROR'
        result['Messages'].append(traceback.format_exc())
    except:
        result['TestStatus'] = 'ERROR'
        result['Messages'].append('Unknown error')
    return json.dumps(result)

    
def run():
    '''
    Tests memory
    '''
    result = {'TestStatus': 'ERROR', 'Messages': []}
    
    # Create array
    LENGTH = 10000000
    values = list(range(0, LENGTH))
    value = 3
    for i in range(0, LENGTH):
        values[i] = value
        value += 3

    # Verify array length
    if len(values) != LENGTH:          
        result['Messages'].append('Error: wrong array length: %d' % len(values))
        return result
    
    # Verify array
    expected_value = 3    
    for i in range(0, LENGTH):
        if values[i] != expected_value:
            result['Messages'].append('Error: Memory failed: values[%d] = %d' % (i, values[i]))
            return result                                                    
        expected_value += 3
    
    result['TestStatus'] = 'SUCCESS'
    return result


if __name__ == '__main__':
    main()
    
