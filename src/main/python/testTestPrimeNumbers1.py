import unittest
import json
import TestPrimeNumbers1

class TestPythonTests(unittest.TestCase):

    def testTestPrimeNumbers1_success(self):
        '''
        '''
        result = json.loads(TestPrimeNumbers1.runTest())
        self.assertEqual(len(result), 2)
        self.assertTrue('TestStatus' in result)
        self.assertTrue('Messages' in result)
        self.assertTrue(result['TestStatus'])
        self.assertTrue(len(result['Messages']) == 8)


if __name__ == '__main__':
    unittest.main()
