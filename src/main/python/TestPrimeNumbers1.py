#                                                                                        
# TestPrimeNumber.py                                                                    
#                                                                                        
# Simple program that tests integer math                                                 
#                                                                                        

import json
import os
import time
import traceback


def main():
    """                                                                                  
    Runs the test case and outputs JSON message to stdout.                               
    """
    print(runTest())


def runTest():
    """                                                                                  
    Runs the test case and handles exceptions.                                           
        """
    if os.name is "nt":
        # On Windows, the best timer is time.clock()                                     
        default_timer = time.clock
    else:
        # On most other platforms the best timer is time.time()                          
        default_timer = time.time

    result = {'TestStatus': 'ERROR', 'Messages': []}
    try:
        start_time = default_timer()
        result = run()
        end_time = default_timer()
        result['Messages'].append('Completed test in %f seconds' % (end_time - start_time))
    except Exception as ex:
        result['TestStatus'] = 'ERROR'
        result['Messages'].append(traceback.format_exc())
    except:
        result['TestStatus'] = 'ERROR'
        result['Messages'].append('Unknown error')
    return json.dumps(result)


def run():
    """                                                                                  
    Finds Prime Numbers                                                                   
    """
    result = {'TestStatus': 'ERROR', 'Messages': []}

    primes = findPrimes(10)
    result['Messages'].append('Found %d primes under 10' % len(primes))

    primes = findPrimes(100)
    result['Messages'].append('Found %d primes under 100' % len(primes))

    primes = findPrimes(1000)
    result['Messages'].append('Found %d primes under 1000' % len(primes))

    primes = findPrimes(10000)
    result['Messages'].append('Found %d primes under 10000' % len(primes))

    primes = findPrimes(100000)
    result['Messages'].append('Found %d primes under 100000' % len(primes))

    primes = findPrimes(1000000)
    result['Messages'].append('Found %d primes under 1000000' % len(primes))

    primes = findPrimes(10000000)
    result['Messages'].append('Found %d primes under 10000000' % len(primes))
 
    result['TestStatus'] = 'SUCCESS'
    return result


def findPrimes(n):
    """
    Sieve of Eratosthenes algorithm to find prime numbers.
    """
    sieve = [True for i in range(n)]
    sieve[0] = False
    sieve[1] = False
    for i in range(2, int(n**(1/2) + 1)):
        if sieve[i]:
            for j in range(i**2, n, i):
                sieve[j] = False
    primes = [i for i in range(len(sieve)) if sieve[i]]
    return primes

if __name__ == '__main__':
    main()
