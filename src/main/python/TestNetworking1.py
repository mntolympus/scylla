#
# TestNetworking1.py
#
# Simple Program that test the network abilities
#

import json
import os
import shutil
import sys
import traceback
import urllib.request
import urllib
import time

def main():

    print(runTest())

def runTest():

    if os.name is "nt":
        default_timer = time.clock
    else:
        default_timer = time.time

    result = {'TestStatus': 'ERROR', 'Messages': []}
    try:
        start_time = default_timer()
        result = run()
        end_time = default_timer()
        result['Messages'].append('Completed test in %f seconds' % (end_time - start_time))
    except Exception as ex:
        result['TestStatus'] = 'ERROR'
        result['Messages'].append(traceback.format_exc())
    except:
        result['TestStatus'] = 'ERROR'
        result['Messages'].append('Unknown error')
    return json.dumps(result)

def run():
    
    result = {'TestStatus': 'ERROR', 'Messages': []}

    # Get a webpage and verify content
    try:
        with urllib.request.urlopen('http://time.gov/') as response:
            html = str(response.read())
            if '<title>The Official NIST US Time:</title>' not in html:
                result['Messages'].append('Failed to verify time.gov')
                return result
    except urllib.error.URLError as ex:
        result['Messages'].append('Failed to load time.gov, URLError: %s' % str(ex))
        return result
    except urllib.error.HTTPError as ex:
        result['Messages'].append('Failed to load time.gov, HTTPError: %s' % str(ex))
        return result


    # Get a webpage and verify content
    try:
        with urllib.request.urlopen('http://google.com/') as response:
            html = str(response.read())
            if '<title>Google</title>' not in html:
                result['Messages'].append('Failed to verify google.com')
                return result
    except urllib.error.URLError as ex:
        result['Messages'].append('Failed to load google.com, URLError: %s' % str(ex))
        return result
    except urllib.error.HTTPError as ex:
        result['Messages'].append('Failed to load google.com, HTTPError: %s' % str(ex))
        return result


    result['TestStatus'] = 'SUCCESS'
    return result

if __name__ == '__main__':
    main()
