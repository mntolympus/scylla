<?php
/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 06-Nov-17
 * Time: 01:28 PM
 */
$files = scandir(__DIR__);
$versions = [];
foreach ($files as $file) {
    $matches = [];
    if (preg_match("/scylla-controller-(\d\.\d\.\d)-*\w*-FULL.jar/", $file, $matches)) {
        $versions[$matches[1]] = $file;
    }
}
arsort($versions);
if (isset($_GET['version'])) {
    $version = filter_var($_GET['version'], FILTER_SANITIZE_STRING);
    if ($version == 'latest') {
        echo array_shift($versions);
    } else {
        echo md5_file($versions[$version]);
    }
} else {
    echo md5_file(array_shift($versions));
}