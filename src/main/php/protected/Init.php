<?php
/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 16-Sep-17
 * Time: 10:58 PM
 */

class Init
{

    public static function initAll()
    {
        //TODO: Add other inits
        self::initErrorHandler();
        self::initDatabase();
    }

    public static function initErrorHandler()
    {
        file_put_contents(__DIR__ . '/log/info.log', "==BEGIN LOG==\n");
        file_put_contents(__DIR__ . '/log/warn.log', "==BEGIN LOG==\n");
        file_put_contents(__DIR__ . '/log/error.log', "==BEGIN LOG==\n");
        file_put_contents(__DIR__ . '/log/fatal.log', "==BEGIN LOG==\n");
    }

    public static function initDatabase()
    {
        require_once __DIR__ . '/Database.php';
        $ddl = file_get_contents(__DIR__ . '/init/hydra.ddl');
        Database::initDB($ddl);
    }

    public static function checkAll(): bool
    {
        //TODO: Add other checks
        if (!self::checkErrorHandler()) {
            echo "ErrorHandler FAIL";
            return false;
        }
        if (!self::checkDatabase()) {
            echo "Database FAIL";
            return false;
        }
        return true;
    }

    public static function checkErrorHandler(): bool
    {
        return file_exists(__DIR__ . '/log/info.log') && file_exists(__DIR__ . '/log/warn.log') && file_exists(__DIR__ . '/log/error.log') && file_exists(__DIR__ . '/log/fatal.log');
    }

    public static function checkDatabase(): bool
    {
        require_once __DIR__ . '/Database.php';
        $db = Database::getDB();
        $status = 0;
        //TODO: Add more other tables
        $status += $db->query("SELECT 1 FROM cosctea3_hydra.execution LIMIT 1") ? 0 : 1;
        $status += $db->query("SELECT 1 FROM cosctea3_hydra.sut LIMIT 1") ? 0 : 1;
        $status += $db->query("SELECT 1 FROM cosctea3_hydra.test LIMIT 1") ? 0 : 1;
        $status += $db->query("SELECT 1 FROM cosctea3_hydra.testsuite LIMIT 1") ? 0 : 1;
        $status += $db->query("SELECT 1 FROM cosctea3_hydra.testsuitetest LIMIT 1") ? 0 : 1;;
        if ($status > 0) {
            Log::warn('Tables not initialized in database!', __LINE__);
            return false;
        }
        return true;
    }
}