CREATE DATABASE IF NOT EXISTS cosctea3_hydra;

USE cosctea3_hydra;

CREATE TABLE cosctea3_hydra.test (
  test_id                    INT PRIMARY KEY AUTO_INCREMENT,
  test_name                  VARCHAR(30) NOT NULL,
  test_script                VARCHAR(30) NOT NULL,
  test_isSerial              BOOL            DEFAULT FALSE,
  test_self_parallel_threads INT             DEFAULT '1',
  test_lifetime              INT             DEFAULT '0'
)
  ENGINE = INNODB;

CREATE TABLE cosctea3_hydra.sut (
  sut_hostname VARCHAR(62) PRIMARY KEY,
  testsuite_id INT
)
  ENGINE = INNODB;

CREATE TABLE cosctea3_hydra.testsuite (
  testsuite_id   INT PRIMARY KEY AUTO_INCREMENT,
  testsuite_name VARCHAR(62)
)
  ENGINE = INNODB;

CREATE TABLE cosctea3_hydra.testsuitetest (
  test_id      INT,
  testsuite_id INT,
  PRIMARY KEY (test_id, testsuite_id),
  testsuitetest_runs INT  DEFAULT '1',
  testsuitetest_delay INT DEFAULT '0'
)
  ENGINE = INNODB;

CREATE TABLE cosctea3_hydra.execution (
  execution_id         INT PRIMARY KEY AUTO_INCREMENT,
  execution_time_start DATETIME        DEFAULT NOW(),
  execution_time_end   DATETIME        DEFAULT NULL,
  execution_success    BOOL            DEFAULT FALSE,
  test_id              INT,
  sut_hostname         VARCHAR(62),
  execution_output     TEXT
)
  ENGINE = INNODB;

ALTER TABLE cosctea3_hydra.execution
  ADD CONSTRAINT execution_test_test_id_fk
FOREIGN KEY (test_id) REFERENCES test (test_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE cosctea3_hydra.execution
  ADD CONSTRAINT execution_sut_sut_hostname_fk
FOREIGN KEY (sut_hostname) REFERENCES sut (sut_hostname) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE cosctea3_hydra.testsuitetest
  ADD CONSTRAINT testsuitetests_test_test_id_fk
FOREIGN KEY (test_id) REFERENCES test (test_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE cosctea3_hydra.testsuitetest
  ADD CONSTRAINT testsuitetests_testsuite_testsuite_id_fk
FOREIGN KEY (testsuite_id) REFERENCES testsuite (testsuite_id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE cosctea3_hydra.sut
  ADD CONSTRAINT sut_testsuite_testsuite_id_fk
FOREIGN KEY (testsuite_id) REFERENCES testsuite (testsuite_id) ON DELETE CASCADE ON UPDATE CASCADE;