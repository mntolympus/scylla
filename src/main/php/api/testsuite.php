<?php

require_once __DIR__ . "/../lib/Response.php";
require_once __DIR__ . "/../repository/TestSuiteRepository.php";

$response = new Response();    
$method = filter_var($_SERVER['REQUEST_METHOD'], FILTER_SANITIZE_STRING);

switch ($method) {
    case 'GET':
        if ($_GET['testSuiteId']) {
            $testSuiteId = filter_var($_GET['testSuiteId'], FILTER_SANITIZE_STRING);
            $testSuiteId = TestSuiteRepository::getTestSuiteWithId($testId);
            $response->pushData($testSuiteId);
            http_response_code(200); // status ok
            $response->echoJSONString();
        } else {
            foreach (TestSuiteRepository::getAllTestSuites() as $testSuite)
                $response->pushData($testSuite);
            http_response_code(200); // status ok
            $response->echoJSONString();
        }
    case 'POST':
        $data = json_decode(filter_var(file_get_contents("php://input"), FILTER_SANITIZE_STRING), true);
        $ret = TestSuiteRepository::insertNewTestSuite($data['testSuiteName']);
        if ($ret)
            http_response_code(201); // status sut created
        else 
            http_response_code(400); // status general error 
        break;
    default: 
        http_response_code(405); // method not found
}

?>