<?php
/**
 * Copyright (c) 2017. Challstrom. All Rights Reserved.
 */

/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 17-Sep-17
 * Time: 05:32 PM
 */

require_once __DIR__ . "/../lib/Response.php";
require_once __DIR__ . "/../repository/TestRepository.php";

$response = new Response();    
$method = filter_var($_SERVER['REQUEST_METHOD'], FILTER_SANITIZE_STRING);
if (isset($_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE'])) $method = filter_var($_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE'], FILTER_SANITIZE_STRING);
else $method = filter_var($_SERVER['REQUEST_METHOD'], FILTER_SANITIZE_STRING);
$response->pushMessage('METHOD: ' . $method);

switch ($method) {
    case 'GET':
        if ($_GET['testId']) {
            $testId = filter_var($_GET['testId'], FILTER_SANITIZE_STRING);
            $test = TestRepository::getTestWithId($testId);
            $response->pushData($test);
            http_response_code(200); // status ok
            $response->echoJSONString();
        } else {
            foreach (TestRepository::getAllTests() as $test)
                $response->pushData($test);
            http_response_code(200); // status ok
            $response->echoJSONString();
        }
        break;
    case 'POST':
        $data = json_decode(file_get_contents("php://input"), true);  
        $ret = TestRepository::insertNewTest($data['testName'], $data['testScript'], $data['testIsSerial'], $data['testNumThreads'], $data['testLifetime']);
        if ($ret)
            http_response_code(201); // status test created
        else 
            http_response_code(400); // status general error 
        break;
    case 'PUT':
        $data = json_decode(file_get_contents("php://input"), true);  
        $ret = TestRepository::updateTest($data['testId'], $data['testName'], $data['testScript'], $data['testIsSerial'], $data['testNumThreads'], $data['testLifetime']);
        if ($ret)
            http_response_code(201); // status test updated
        else 
            http_response_code(400); // status general error 
        break;
    case 'DELETE':
        $data = json_decode(file_get_contents("php://input"), true);  
        $ret = TestRepository::deleteTest($data['testId']);
        if ($ret)
            http_response_code(201); // status test delete
        else 
            http_response_code(400); // status general error 
        break;
    case 'FILE':
        if (isset($_GET['testScript']) && $_GET['testScript'] !== '') {
            $testScript = filter_var(urldecode($_GET['testScript']));
            $testDir = '../test/' . $testScript;
            if (file_exists($testDir)) {
                http_response_code(200); // status ok
                header('Content-Description: File Transfer');
                header('Content-Type: application/octet-stream');
                header('Content-Disposition: attachment; filename="' . basename($testScript) . '"');
                header('Expires: 0');
                header('Cache-Control: must-revalidate');
                header('Pragma: public');
                header('Content-Length: ' . filesize($testDir));
                readfile($testDir);
            } else {
                http_response_code(404); // status python script not found
                echo "ERROR! File: $testScript could not be found!";
            }
        }
        else {
            http_response_code(422); // status missing argument
        }
        break;
    default: 
        http_response_code(405); // method not found
}

?>