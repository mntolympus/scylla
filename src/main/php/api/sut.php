<?php

require_once __DIR__ . "/../lib/Response.php";
require_once __DIR__ . "/../repository/SutRepository.php";
require_once __DIR__ . '/../protected/Log.php';

$response = new Response();    
$method = filter_var($_SERVER['REQUEST_METHOD'], FILTER_SANITIZE_STRING);

switch ($method) {
    case 'GET':
        if ($_GET['sutHostname']) {
            $hostname = filter_var($_GET['sutHostname'], FILTER_SANITIZE_STRING);
            $sut = SutRepository::getSutWithHostname($hostname);
            $response->pushData($sut);
            http_response_code(200); // status ok
            $response->echoJSONString();
        } else {
            foreach (SutRepository::getAllSuts() as $sut)
            $response->pushData($sut);
            http_response_code(200); // status ok
            $response->echoJSONString();
        }
        break;
    case 'POST':
        $data = json_decode(file_get_contents("php://input"), true);  
        $ret = SutRepository::insertNewSut($data['sutHostname'], $data['testSuiteId']);
        if ($ret)
            http_response_code(201); // status sut created
        else 
            http_response_code(400); // status general error 
        break;
    case 'PUT':
        $data = json_decode(file_get_contents("php://input"), true);  
        $ret = SutRepository::updateSut($data['sutHostname'], $data['testSuiteId']);
        if ($ret)
            http_response_code(201); // status sut updated
        else 
            http_response_code(400); // status general error 
        break;
    case 'DELETE':
        $data = json_decode(file_get_contents("php://input"), true);  
        Log::info("DELETE DATA: " . file_get_contents("php://input"), __LINE__);
        $ret = SutRepository::deleteSut($data['sutHostname']);
        if ($ret)
            http_response_code(201); // status sut deleted
        else 
            http_response_code(400); // status general error 
        break;
    default: 
        http_response_code(405); // method not found
}

?>