# API Reference

This document contains the API specification for Scylla. The following are API endpoint scripts, their inputs, outputs, and available https methods.

The default return is a 405 status code for unsupported methods, and 204 status code for unimplemented methods.

Example API Payloads can be replayed using [Insomnia](https://insomnia.rest) and the provided [insomnia export](api-export.json).

---

## a) test.php

`test.php` contains three endpoints for interacting with the Scylla test object.

### a.1) GET `test.php`

Return a(ll) Test record(s).

| Input Key | Input Value | Return Value | Complete |
| ---       | ---         | ---          | --- |
null | null | JSON List of all tests. | **needs db**
testId | Integer | Test attributes by key | yes

### a.2) POST `test.php`

Create a new test.

| Input Key | Input Value | Return Value | Complete |
| ---       | ---         | ---          | --- |
*https-body* | Object mapped json | 201 or 400 | **needs db**

### a.3) FILE `test.php`

Download a test script by name. Right now, assume python.


| Input Key | Input Value | Return Value | Complete |
| ---       | ---         | ---          | --- |
testScriptName | String | Test script file transfer | **yes**

---

## b) sut.php

`sut.php` contains two endpoints for interacting with the Scylla sut object.

### b.1) GET `sut.php`

Return a(ll) Sut record(s).

| Input Key | Input Value | Return Value | Complete |
| ---       | ---         | ---          | --- |
null | null | JSON List of all suts. | **needs db**
sutHostname | String | Sut attributes by key | **needs db**

### b.2) POST `sut.php`

Create a new SUT.

| Input Key | Input Value | Return Value | Complete |
| ---       | ---         | ---          | --- |
*https-body* | Object mapped json | 201 or 400 | **needs db**

---

## c) testsuite.php

`testsuite.php` contains two endpoints for interacting with the Scylla TestSuite object.

### c.1) GET `testsuite.php`

Return a(ll) Test Suite record(s).

| Input Key | Input Value | Return Value | Complete |
| ---       | ---         | ---          | --- |
null | null | JSON List of all Test Suites. | **needs db**
testSuiteId | Integer | Test Suite attributes by key | **needs db**

### c.2) POST `testsuite.php`

Create a new Test Suite.

| Input Key | Input Value | Return Value | Complete |
| ---       | ---         | ---          | --- |
*https-body* | Object mapped json | 201 or 400 | **needs db**

---

## d) execute.php

`execute.php` contains a strict workflow for the lifetime of an sut test suite execution.

### d.1) GET `execute.php`

First step of execution. Get the test suite associated with a sut hostname.

| Input Key | Input Value | Return Value | Complete |
| ---       | ---         | ---          | --- |
null | null | JSON List of all test results. | **needs db**
sutHostname | String | 200 w/ List of tests from a test suite, or 404, or 400. | **needs db**

### d.2) POST `execute.php`

Second step of execution. Use the hostname and a test returned from above to create a new execution record.

| Input Key | Input Value | Return Value | Complete |
| ---       | ---         | ---          | --- |
*https-body* | Json w/ sut hostname and test id. | 200 w/ exec id or 400 | **needs db**

### d.3) PUT `execute.php`

Last step of execution. Use the test response and id from above to close out an open execution record.

| Input Key | Input Value | Return Value | Complete |
| ---       | ---         | ---          | --- |
*https-body* | Json w/ exec id and success bool. | 201 or 400 | **needs db**