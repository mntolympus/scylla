<?php
/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 30-Nov-17
 * Time: 08:11 PM
 */

$logDir = __DIR__ . "/../executionLogs/";


$method = filter_var($_SERVER['REQUEST_METHOD'], FILTER_SANITIZE_STRING);
if (isset($_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE'])) $method = filter_var($_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE'], FILTER_SANITIZE_STRING);
switch ($method) {
    case 'POST':
        $data = json_decode(file_get_contents("php://input"), true);
        if (isset($data['executionId']) && isset($data['log'])){
            $executionId = filter_var($data['executionId'], FILTER_SANITIZE_STRING);
            $log = filter_var($data['log'], FILTER_SANITIZE_STRING);
            $logLines = explode(PHP_EOL, $log);
            foreach ($logLines as $line => $logLine) {
                $logLines[$line] = $line . ":" . $logLine;
            }
            file_put_contents($logDir . "/$executionId.log", implode(PHP_EOL, $logLines));
        } else {
            http_response_code(422);
        }
        break;
    default:
        http_response_code(405);
}