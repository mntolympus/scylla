<?php
/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 01-Dec-17
 * Time: 02:11 PM
 */

$key = '7feabcf3cf642e4ee546a950a60b43a4';

if (isset($_GET['key'])) {
    if ($key == $_GET['key']) {
        $logDir = __DIR__ . "/../executionLogs/";
        $digest = file_get_contents($logDir.'/digest.log');
        $date = date('d/m/Y', time());
        $message = "==========BEGIN DIGEST==========\n";
        $message .= $digest;
        $message .= "\n==========END DIGEST==========\n";
        mail("tchallst@stedwards.edu", "Scylla CIC Daily Digest on $date", $message);
        print_r($message);
        file_put_contents($logDir.'/digest.log', "", LOCK_EX);
    } else {
        http_response_code(403);
    }
} else {
    http_response_code(422);
}