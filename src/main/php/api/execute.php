<?php

require_once __DIR__ . "/../lib/Response.php";
require_once __DIR__ . "/../repository/SutRepository.php";
require_once __DIR__ . "/../repository/ExecutionRepository.php";

$response = new Response();    
$method = filter_var($_SERVER['REQUEST_METHOD'], FILTER_SANITIZE_STRING);
if (isset($_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE'])) $method = filter_var($_SERVER['HTTP_X_HTTP_METHOD_OVERRIDE'], FILTER_SANITIZE_STRING);
$response->pushMessage('METHOD: ' . $method);
switch ($method) {
    case 'GET':
        if ($_GET['sutHostname']) {
            $hostname = filter_var($_GET['sutHostname'], FILTER_SANITIZE_STRING);
            $sut = SutRepository::getSutWithHostname($hostname);
            if ($sut->getSutHostname() == ''){
                http_response_code(404); // sut not found                
            } else {
                $testCases = ExecutionRepository::getTestsForTestSuiteId($sut->getTestSuiteId());
                foreach ($testCases as $testCase)
                    $response->pushData($testCase);
                http_response_code(200); // status ok, print all tests        
                $response->echoJSONString();
            }
        } else {
            foreach (ExecutionRepository::getAllResults() as $result)
                $response->pushData($result);
            http_response_code(200); // status ok, print all tests        
            $response->echoJSONString();
        }
        break;
    case 'POST':
        $data = json_decode(file_get_contents("php://input"), true);
        $ret = ExecutionRepository::insertNewExecution($data['sutHostname'], $data['testId']);
        if ($ret){
            $response->pushData($ret);
            http_response_code(200); // status ok, print exec id     
            $response->echoJSONString();
        }
        else 
            http_response_code(400); // status general error 
        break;
    case 'PUT':
        $data = json_decode(file_get_contents("php://input"), true);
        $ret = ExecutionRepository::addExecutionStatus($data['executionId'], $data['success'], $data['output']);
        if ($data['success'] == '0') {
            $logDir = __DIR__ . "/../executionLogs/";
            $message = $data['executionId'] . '    ' .$data['success'] .'    ' . $data['output'] . PHP_EOL;
            file_put_contents($logDir . '/digest.log', $message, FILE_APPEND | LOCK_EX);
        }
        if ($ret)
            http_response_code(201); // status ok, no content      
        else 
            http_response_code(400); // status general error 
        break;
    default: 
        http_response_code(405); // method not found
}
?>