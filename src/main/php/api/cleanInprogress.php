<?php
/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 07-Nov-17
 * Time: 01:03 PM
 */

require_once __DIR__ . '/../protected/Database.php';

Database::runQueryVoid("DELETE FROM cosctea3_hydra.execution WHERE execution_success = FALSE AND execution_time_end IS NULL AND NOW() > DATE_ADD(execution_time_start, INTERVAL 30 SECOND)");
