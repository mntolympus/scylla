<?php

class TestSuite implements JsonSerializable {

    private $testSuiteId;
    private $testSuiteName;

    public function __construct($testSuiteId, $testSuiteName) {
        $this->testSuiteId = $testSuiteId;
        $this->testSuiteName = $testSuiteName;
    }

    public function getTestSuiteId(): int {
        return $this->testSuiteId;
    }

    public function getTestSuiteName(): string {
        return $this->testSuiteName;
    }

    public function jsonSerialize(): array {
        return get_object_vars($this);
    }
}