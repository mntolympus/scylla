<?php

require_once __DIR__ . "/../model/Test.php";

class TestSuiteTest implements JsonSerializable {

    private $test;
    private $testSuiteTestRuns;
    private $testSuiteTestDelay;

    public function __construct($test, $testSuiteTestRuns, $testSuiteTestDelay) {
        $this->test = $test;
        $this->testSuiteTestRuns = $testSuiteTestRuns;
        $this->testSuiteTestDelay = $testSuiteTestDelay;
    }

    public function getTest(): Test {
        return $this->test;
    }

    public function getTestSuiteTestRuns(): string {
        return $this->testSuiteTestRuns;
    }

    public function getTestSuiteTestDelay(): string {
        return $this->testSuiteTestDelay;
    }
    
    public function jsonSerialize(): array {
        return get_object_vars($this);
    }
}