<?php

class Execution implements JsonSerializable {

    private $executionId;
    private $executionTimeStart;
    private $executionTimeEnd;
    private $executionSuccess;
    private $testName;
    private $sutHostname;
    private $output;

    public function __construct($executionId, $executionTimeStart, $executionTimeEnd, $executionSuccess, $testName, $sutHostname, $output) {
        $this->executionId = $executionId;
        $this->executionTimeStart = $executionTimeStart;
        $this->executionTimeEnd = $executionTimeEnd;
        $this->executionSuccess = $executionSuccess;
        $this->testName = $testName;
        $this->sutHostname = $sutHostname;
        $this->output = $output;
    }

    public function getExecutionId(): Int {
        return $this->$executionId;
    }

    public function getExecutionTimeStart(): DateTime {
        return $this->$executionTimeStart;
    }

    public function getExecutionTimeEnd(): DateTime {
        return $this->$executionTimeEnd;
    }

    public function getExecutionSuccess(): Bool {
        return $this->$executionSuccess;
    }

    public function getTestName(): Int {
        return $this->$testName;
    }
    
    public function getSutHostname(): String {
        return $this->$sutHostname;
    }

    public function getOutput(): String {
        return $this->$output;
    }

    public function jsonSerialize(): array {
        return get_object_vars($this);
    }
}