<?php

class Sut implements JsonSerializable {

    private $sutHostname;
    private $testSuiteId;

    public function __construct($sutHostname, $testSuiteId) {
        $this->sutHostname = $sutHostname;
        $this->testSuiteId = $testSuiteId;
    }

    public function getSutHostname(): string {
        return $this->sutHostname;
    }

    public function getTestSuiteId(): string {
        return $this->testSuiteId;
    }

    public function jsonSerialize(): array {
        return get_object_vars($this);
    }
}