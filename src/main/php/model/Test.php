<?php
/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 17-Sep-17
 * Time: 05:33 PM
 */

class Test implements JsonSerializable {

    private $testId;
    private $testName;
    private $testScript;
    private $testIsSerial;
    private $testNumThreads;
    private $testLifetime;

    public function __construct($testId, $testName, $testScript, $testIsSerial, $testNumThreads, $testLifetime) {
        $this->testId = $testId;
        $this->testName = $testName;
        $this->testScript = $testScript;
        $this->testIsSerial = $testIsSerial;
        $this->testNumThreads = $testNumThreads;
        $this->testLifetime = $testLifetime;
    }

    public function getTestId(): int {
        return $this->testId;
    }

    public function getTestName(): string {
        return $this->testName;
    }

    public function getTestScript(): string {
        return $this->testScript;
    }

    public function getTestIsSerial(): bool {
        return $this->testIsSerial;
    }

    public function getTestNumThreads(): int {
        return $this->testNumThreads;
    }

    public function getTestLifetime(): int {
        return $this->testLifetime;
    }

    public function jsonSerialize(): array {
        return get_object_vars($this);
    }
}