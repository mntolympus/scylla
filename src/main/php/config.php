<?php
/**
 * Created by IntelliJ IDEA.
 * User: tchallst
 * Date: 16-Sep-17
 * Time: 11:18 PM
 */

$admin = json_decode(file_get_contents(__DIR__ . "/protected/config/admin.json"), true);

if (isset($_POST['submit'])) {
    if (isset($_POST['username']) && $admin['username'] === $_POST['username'] && isset($_POST['password']) && $admin['password'] === $_POST['password']) {
        echo "Welcome Admin!";
        require_once __DIR__ . "/protected/Init.php";
        if (!Init::checkAll()) {
            echo "Warning! Initializing new System!";
            Init::initAll();
        }

    }
}

?>

<form action="config.php" method="post">
    <label for="username">Username: </label>
    <input name="username" id="username" type="text" autofocus>
    <label for="password">Password: </label>
    <input name="password" id="password" type="password">
    <button name="submit" type="submit">Submit</button>
</form>
