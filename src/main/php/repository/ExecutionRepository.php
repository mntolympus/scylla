<?php

require_once __DIR__ . "/../model/Execution.php";
require_once __DIR__ . "/../model/Test.php";
require_once __DIR__ . "/../model/TestSuiteTest.php";
require_once __DIR__ . "/../protected/Database.php";
require_once __DIR__ . "/../repository/TestRepository.php";

class ExecutionRepository {

    public static function getTestsForTestSuiteId($testSuiteId): Array {
        $testSuiteTests = [];
        $dataArray = Database::runQueryAll("SELECT * FROM cosctea3_hydra.testsuitetest WHERE testsuite_id = '$testSuiteId'");
        foreach ($dataArray as $data) {
            $test = TestRepository::getTestWithId($data['test_id']);
            array_push($testSuiteTests, new TestSuiteTest($test, $data['testsuitetest_runs'], $data['testsuitetest_delay']));
        }
        return $testSuiteTests;
    }

    public static function getAllResults(): Array {
        $results = [];
        $dataArray = Database::runQueryAll("SELECT * FROM cosctea3_hydra.execution");
        foreach ($dataArray as $data) {
            $testName = TestRepository::getTestWithId($data['test_id'])->getTestName();
            $startTime = (new DateTime($data['execution_time_start']))->format('U');
            $endTime = $data['execution_time_end'] != '' ? (new DateTime($data['execution_time_end']))->format('U') : 0;
            array_push($results, new Execution($data['execution_id'], $startTime, $endTime, boolval($data['execution_success']), $testName, $data['sut_hostname'], $data['output']));
        }
        return $results;
    }

    public static function insertNewExecution($hostname, $testId): int {
        $ret = Database::runQuerySingle("INSERT INTO cosctea3_hydra.execution (test_id, sut_hostname) VALUES ('$testId', '$hostname')");
        return $ret == true ? Database::getLastKey() : null;
    }

    public static function addExecutionStatus($execId, $success, $output): bool {
        $time = date('Y-m-d H:i:s');
        $output = Database::scrubQuery($output);
        return Database::runQuerySingle("UPDATE cosctea3_hydra.execution SET execution_success = '$success', execution_time_end = '$time', output = '$output' WHERE execution_id = '$execId'");
        
    }
}
?>