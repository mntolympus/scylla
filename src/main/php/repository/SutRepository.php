<?php

require_once __DIR__ . "/../model/Sut.php";
require_once __DIR__ . "/../protected/Database.php";

class SutRepository {

    public static function getAllSuts(): Array {
        $suts = [];
        $dataArray = Database::runQueryAll("SELECT * FROM cosctea3_hydra.sut");
        foreach ($dataArray as $data)
            array_push($suts, new Sut($data['sut_hostname'], $data['testsuite_id']));
        return $suts;
    }

    public static function getSutWithHostname($hostName): Sut {
        $data = Database::runQuerySingle("SELECT * FROM cosctea3_hydra.sut WHERE sut_hostname = '$hostName'");
        return new Sut($data['sut_hostname'], $data['testsuite_id']);
    }

    public static function insertNewSut($hostname, $testSuiteId): bool {
    	
    	return Database::runQuerySingle("INSERT INTO cosctea3_hydra.sut (sut_hostname, testsuite_id) VALUES ('$hostname','$testSuiteId')");
    }
    
    public static function deleteSut($hostname): bool {
    	
    	return Database::runQuerySingle("DELETE FROM cosctea3_hydra.sut WHERE sut_hostname = '$hostname'");
    }
    
    public static function updateSut($hostname, $testSuiteId): bool {
    	
    	return Database::runQuerySingle("UPDATE cosctea3_hydra.sut SET testsuite_id = '$testSuiteId' WHERE sut_hostname = '$hostname'");
    	
    }
    
}
?>