<?php

require_once __DIR__ . "/../model/Test.php";
require_once __DIR__ . "/../protected/Database.php";

class TestRepository {

    public static function getAllTests(): Array {
        $tests = [];
        $dataArray = Database::runQueryAll("SELECT * FROM cosctea3_hydra.test");
        foreach ($dataArray as $data)
            array_push($tests, new Test($data['test_id'], $data['test_name'], $data['test_script'], boolval($data['test_isSerial']), 
            $data['test_self_parallel_threads'], $data['test_lifetime']));
        return $tests;
    }

    public static function insertNewTest($testName, $testScriptName, $testIsSerial, $testNumThreads, $testLifetime): bool {
    	
    	return Database::runQuerySingle("INSERT INTO cosctea3_hydra.test (test_name, test_script, test_isSerial, test_self_parallel_threads, test_lifetime) VALUES ('$testName','$testScriptName','$testIsSerial','$testNumThreads','$testLifetime')");
    }
    
    public static function deleteTest($testId): bool {
    	return Database::runQuerySingle("DELETE FROM cosctea3_hydra.test WHERE test_id = '$testId'");
    }
    
    public static function updateTest($testId, $testName, $testScriptName, $testIsSerial, $testNumThreads, $testLifetime): bool {
    	
    	return Database::runQuerySingle("UPDATE cosctea3_hydra.test SET test_name = '$testName', test_script = '$testScriptName', test_isSerial = '$testIsSerial', test_self_parallel_threads = '$testNumThreads', test_lifetime = '$testLifetime' WHERE test_id = '$testId'");
    	
    }
    
    public static function getTestWithId($testId): Test {
        $data = Database::runQuerySingle("SELECT * FROM cosctea3_hydra.test WHERE test_id = '$testId'");
        return new Test($data['test_id'], $data['test_name'], $data['test_script'], boolval($data['test_isSerial']), 
            $data['test_self_parallel_threads'], $data['test_lifetime']);
    }
    
    
}
?>