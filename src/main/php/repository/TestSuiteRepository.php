<?php

require_once __DIR__ . "/../model/TestSuite.php";
require_once __DIR__ . "/../protected/Database.php";

class TestSuiteRepository {

    public static function getAllTestSuites(): Array {
        $testSuites = [];
        $dataArray = Database::runQueryAll("SELECT * FROM cosctea3_hydra.testsuite");
        foreach ($dataArray as $data)
            array_push($testSuites, new TestSuite($data['testsuite_id'], $data['testsuite_name']));
        return $testSuites;    }

    public static function getTestSuiteWithId($testSuiteId): TestSuite {
        $data = Database::runQuerySingle("SELECT * FROM cosctea3_hydra.testsuite");
        return new TestSuite($data['testsuite_id'], $data['testsuite_name']);
    }

    public static function insertNewTestSuite($testSuiteName): bool {
  
    	return Database::runQuerySingle("INSERT INTO cosctea3_hydra.testsuite (testsuite_name) VALUES ('$testSuiteName')");
    }
    
    //Not needed but here for potential future use
    public static function deleteTestSuite($testSuiteId): bool {
    	
    	return Database::runQuerySingle("DELETE FROM cosctea3_hydra.testsuite WHERE testsuite_id = '$testSuiteId'");
    }
    
    //Not needed but here for potential future use
    public static function updateTestSuite($testSuiteId, $testSuiteName): bool {
    	
    	return Database::runQuerySingle("UPDATE cosctea3_hydra.testsuite SET testsuite_name = '$testSuiteName' WHERE testsuite_id = '$testSuiteId'");
    	
    }
}
?>