/*
 * Copyright (c) 2017. Challstrom. All Rights Reserved.
 */

package com.cosc4345.hydra;

import com.cosc4345.hydra.request.*;
import com.cosc4345.hydra.view.UIController;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Web implements Runnable {
    public static final long webPollRate = 1000;
    //http://cosc4345-team4.com/
    //make sure to include protocol
    private static final String baseURL = "http://cosc4345-team4.com/";
    private static Web instance;
    private final ConcurrentLinkedQueue<Request> requestStack;

    private Web() {
        requestStack = new ConcurrentLinkedQueue<>();
    }

    public static Web getInstance() {
        if (instance == null) {
            instance = new Web();
        }
        return instance;
    }

    @Override
    public void run() {
        while (Main.systemIsRunning) {
            if (requestStack.size() > 0) {
                //TODO: clean up this shite
                Request request = requestStack.remove();
                //dequeue and handle request
                try {
                    URL url = new URL(baseURL + request.getSubResource());
                    if (request instanceof GetRequest) {
                        url = new URL(url.toString() + ((GetRequest) request).getEncodedGetData());
                    }
                    UIController.println("Requesting " + url.toString() + " via " + request.getClass().getSimpleName());
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    if (request instanceof PostRequest) {
                        String body = ((PostRequest) request).getBody();
                        //This automatically calls setRequestMethod("POST");
                        connection.setDoOutput(true);
                        if (request instanceof PutRequest) {
                            connection.addRequestProperty("X-HTTP-Method-Override", "PUT");
                            connection.setRequestMethod("PUT");
                        }
                        connection.addRequestProperty("Content-Type", "application/JSON");
                        connection.addRequestProperty("Content-Length", Integer.toString(body.length()));
                        connection.getOutputStream().write(body.getBytes("UTF-8"));
                    }
                    if (request instanceof FileRequest) {
                        connection.setDoOutput(true);
                        connection.setRequestMethod("GET");
                        connection.addRequestProperty("X-HTTP-Method-Override", "FILE");
                    }
                    //Connection connect() is fired with getInputStream()
                    BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
                    UIController.println("Status: " + connection.getResponseMessage());
                    StringBuilder dataString = new StringBuilder();
                    int dataChar;
                    while ((dataChar = reader.read()) != -1) {
                        dataString.append((char) dataChar);
                    }
                    request.setOutput(dataString.toString());
                    if (request instanceof FileRequest) {
                        UIController.println("Receiving binary file transfer of " + ((FileRequest) request).getFilename());
                        FileHelper.writeStringToFile(((FileRequest) request).getFilename(), request.getOutput());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    Thread.sleep(webPollRate);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void queueRequest(Request request) {
        requestStack.add(request);
    }
}
