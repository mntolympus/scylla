package com.cosc4345.hydra.request;

import java.util.HashMap;

public class PutRequest extends PostRequest {

    public PutRequest(String subResource, HashMap<String, String> postRequestData) {
        super(subResource, postRequestData);
    }
}
