package com.cosc4345.hydra.request;

import com.google.gson.Gson;

import java.util.HashMap;

public class PostRequest extends Request {
    private HashMap<String, String> postRequestData;

    public PostRequest(String subResource, HashMap<String, String> postRequestData) {
        super(subResource);
        this.postRequestData = postRequestData;
    }

    public String getBody() {
        Gson gson = new Gson();
        return gson.toJson(postRequestData);
    }
}
