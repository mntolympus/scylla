package com.cosc4345.hydra;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by TJ Challstrom on 30-Nov-17 at 09:02 PM.
 * Dispenser goes here!
 */
public class Log implements Runnable {
    private static Log instance;
    private File currentFile;
    private FileOutputStream outputStream;
    private int currentLine;
    private String baseName;
    private int filesWritten;
    private ConcurrentLinkedQueue<String> lines;

    private Log() {
        lines = new ConcurrentLinkedQueue<>();
        baseName = Version.getVersionString() + "_" + (int) (System.currentTimeMillis() / 1000L);
        currentLine = 0;
        filesWritten = 0;
        updateCurrentFile();
        try {
            outputStream = new FileOutputStream(currentFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static Log getInstance() {
        if (instance == null){
            instance = new Log();
        }
        return instance;
    }

    private void updateCurrentFile() {
        currentFile = new File(baseName + "." + filesWritten + ".log");
    }

    private void checkFile() {
        if (currentFile.length() > 1*1000000) {
            try {
                outputStream.close();
                filesWritten++;
                updateCurrentFile();
                outputStream = new FileOutputStream(currentFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void log(String message) {
        lines.add(message);
    }

    @Override
    public void run() {
        while (Main.systemIsRunning) {
            checkFile();
            if (lines.size() > 0) {
                try {
                    outputStream.write((currentLine + ":\t" + lines.remove() + '\n').getBytes());
                    currentLine++;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            try {
                Thread.sleep(Main.waitPollRate);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if (lines.size() > 0) {
            lines.forEach(line -> {
                try {
                    outputStream.write((currentLine + ":\t" + lines.remove() + '\n').getBytes());
                    currentLine++;
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        }
        try {
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
