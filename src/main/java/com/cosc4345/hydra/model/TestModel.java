package com.cosc4345.hydra.model;

@SuppressWarnings("ALL")
public class TestModel {
    private int testId;
    private String testName;
    private String testScript;
    private boolean testIsSerial;
    private int testNumThreads;
    private int testLifetime;

    public int getTestId() {
        return testId;
    }

    public String getTestName() {
        return testName;
    }

    public String getTestScript() {
        return testScript;
    }

    public boolean isTestIsSerial() {
        return testIsSerial;
    }

    public int getTestNumThreads() {
        return testNumThreads;
    }

    public int getTestLifetime() {
        return testLifetime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TestModel testModel = (TestModel) o;

        if (testId != testModel.testId) return false;
        if (testIsSerial != testModel.testIsSerial) return false;
        if (testNumThreads != testModel.testNumThreads) return false;
        if (testLifetime != testModel.testLifetime) return false;
        return testName.equals(testModel.testName) && testScript.equals(testModel.testScript);
    }

    @Override
    public int hashCode() {
        int result = testId;
        result = 31 * result + testName.hashCode();
        result = 31 * result + testScript.hashCode();
        result = 31 * result + (testIsSerial ? 1 : 0);
        result = 31 * result + testNumThreads;
        result = 31 * result + testLifetime;
        return result;
    }

    @Override
    public String toString() {
        return "TestModel{" +
                "testId=" + testId +
                ", testName='" + testName + '\'' +
                ", testScript='" + testScript + '\'' +
                ", testIsSerial=" + testIsSerial +
                ", testNumThreads=" + testNumThreads +
                ", testLifetime=" + testLifetime +
                '}';
    }
}
