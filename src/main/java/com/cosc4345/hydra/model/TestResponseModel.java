package com.cosc4345.hydra.model;

import java.util.List;

//TODO: generify
public class TestResponseModel {
    private int errorCount;
    private List<String> messages;
    private String status;
    private List<TestSuiteTestModel> data;

    public int getErrorCount() {
        return errorCount;
    }

    public List<String> getMessages() {
        return messages;
    }

    public String getStatus() {
        return status;
    }

    public List<TestSuiteTestModel> getData() {
        return data;
    }

    @Override
    public String toString() {
        return "TestResponseModel{" +
                "errorCount=" + errorCount +
                ", messages=" + messages +
                ", status='" + status + '\'' +
                ", data=" + data +
                '}';
    }
}
