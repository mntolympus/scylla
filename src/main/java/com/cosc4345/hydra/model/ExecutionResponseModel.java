package com.cosc4345.hydra.model;

import java.util.List;

public class ExecutionResponseModel {
    private int errorCount;
    private List<String> messages;
    private String status;
    private List<Integer> data;

    public int getErrorCount() {
        return errorCount;
    }

    public List<String> getMessages() {
        return messages;
    }

    public String getStatus() {
        return status;
    }

    public List<Integer> getData() {
        return data;
    }

    @Override
    public String toString() {
        return "ExecutionResponseModel{" +
                "errorCount=" + errorCount +
                ", messages=" + messages +
                ", status='" + status + '\'' +
                ", data=" + data +
                '}';
    }
}
