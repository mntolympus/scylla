package com.cosc4345.hydra.model;

@SuppressWarnings("unused")
public class TestSuiteTestModel {
    private TestModel test;
    private int testSuiteTestRuns;
    private int testSuiteTestDelay;

    public TestModel getTest() {
        return test;
    }

    public int getTestSuiteTestRuns() {
        return testSuiteTestRuns;
    }

    public int getTestSuiteTestDelay() {
        return testSuiteTestDelay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TestSuiteTestModel that = (TestSuiteTestModel) o;

        return testSuiteTestRuns == that.testSuiteTestRuns && testSuiteTestDelay == that.testSuiteTestDelay && test.equals(that.test);
    }

    @Override
    public int hashCode() {
        int result = test.hashCode();
        result = 31 * result + testSuiteTestRuns;
        result = 31 * result + testSuiteTestDelay;
        return result;
    }

    @Override
    public String toString() {
        return "TestSuiteTestModel{" +
                "test=" + test +
                ", testSuiteTestRuns=" + testSuiteTestRuns +
                ", testSuiteTestDelay=" + testSuiteTestDelay +
                '}';
    }
}
