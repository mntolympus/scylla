package com.cosc4345.hydra.model;

import java.util.List;

public class PythonResponseModel {
    private List<String> Messages;
    private String TestStatus;

    public List<String> getMessages() {
        return Messages;
    }

    public String getTestStatus() {
        return TestStatus;
    }
}
