/*
 * Copyright (c) 2017. Challstrom. All Rights Reserved.
 */

package com.cosc4345.hydra.test;

import com.cosc4345.hydra.Main;
import com.cosc4345.hydra.model.TestModel;
import com.cosc4345.hydra.model.TestSuiteTestModel;

import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

public class TestExecutor implements Runnable {
    private static TestExecutor instance;
    private final ExecutorService threadPool;
    private final ConcurrentLinkedQueue<ScheduledTest> scheduledTests;
    AtomicInteger serialTestsRunning;
    private AtomicInteger serialTestId;

    private TestExecutor() {
        serialTestsRunning = new AtomicInteger(0);
        serialTestId = new AtomicInteger(0);
        ThreadFactory testThreadFactory = new TestThreadFactory();
        threadPool = Executors.newCachedThreadPool(testThreadFactory);
        scheduledTests = new ConcurrentLinkedQueue<>();
    }

    public static TestExecutor getInstance() {
        if (instance == null) {
            instance = new TestExecutor();
        }
        return instance;
    }

    public void schedule(TestSuiteTestModel testSuiteTest) {
        TestModel test = testSuiteTest.getTest();
        //calculate how many runnable tests to generate

        for (int i = 0; i < testSuiteTest.getTestSuiteTestRuns(); i++) {
            for (int j = 0; j < test.getTestNumThreads(); j++) {
                RunnableTest runnableTest = new RunnableTest(test, i + "-" + j);
                int currentEpoch = (int) (System.currentTimeMillis() / 1000L);
                int epochToRun = currentEpoch + (testSuiteTest.getTestSuiteTestDelay()*i+1);
                ScheduledTest scheduledTest = new ScheduledTest(runnableTest, epochToRun);
                scheduledTests.add(scheduledTest);
            }
        }
    }

    private void submitSerialTest(RunnableTest test) {
        serialTestsRunning.getAndIncrement();
        serialTestId.set(test.getTestId());
        threadPool.submit(test);
    }

    private void submitParallelTest(RunnableTest test) {
        threadPool.submit(test);
    }

    @Override
    public void run() {
        while (Main.systemIsRunning) {
            //TODO: figure out how to reset serial tests
            //go through scheduled tests and try to run
            scheduledTests.forEach((scheduledTest -> {
                if (scheduledTest.isReady()) {
                    //We found a test that's ready!
                    if (serialTestsRunning.get() > 0) {
                        //wait! there's a serial testing running!
                        if (serialTestId.get() == scheduledTest.getTest().getTestId()) {
                            //there's a serial test running, but it's the same type - so go for it!
                            //we can assume if we're here then the test is serial
                            submitSerialTest(scheduledTest.getTest());
                            scheduledTests.remove(scheduledTest);
                        }
                        //else: there's a serial test running of the wrong type
                    } else {
                        //no serial test running, go ahead and schedule
                        if (scheduledTest.getTest().isTest_isSerial()) {
                            submitSerialTest(scheduledTest.getTest());
                        } else {
                            submitParallelTest(scheduledTest.getTest());
                        }
                        scheduledTests.remove(scheduledTest);
                    }
                }
            }));
            try {
                Thread.sleep(Main.waitPollRate);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void shutdown() {
        threadPool.shutdown();
    }
}
