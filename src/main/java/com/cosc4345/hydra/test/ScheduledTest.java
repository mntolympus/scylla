package com.cosc4345.hydra.test;

//TODO: collapse this downward
public class ScheduledTest {
    private RunnableTest runnableTest;
    private int epoch;

    ScheduledTest(RunnableTest runnableTest, int epoch) {
        this.runnableTest = runnableTest;
        this.epoch = epoch;
    }

    public RunnableTest getTest() {
        return runnableTest;
    }


    private boolean isTime() {
        return epoch < (int) (System.currentTimeMillis() / 1000L);
    }

    boolean isReady() {
        return isTime();
    }
}
