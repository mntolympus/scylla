package com.cosc4345.hydra.test;

import com.cosc4345.hydra.Main;
import com.cosc4345.hydra.Web;
import com.cosc4345.hydra.model.TestResponseModel;
import com.cosc4345.hydra.model.TestSuiteTestModel;
import com.cosc4345.hydra.request.FileRequest;
import com.cosc4345.hydra.request.GetRequest;
import com.cosc4345.hydra.request.Request;
import com.cosc4345.hydra.view.UIController;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Locale;
import java.util.Set;

public class TestScheduler implements Runnable {
    private final TestSuite testSuite = new TestSuite();

    @Override
    public void run() {
        while (Main.systemIsRunning) {
            //make requests
            HashMap<String, String> executeRequestMap = new HashMap<>();
            Runtime rt = Runtime.getRuntime();
            Process proc = null;
            try {
                proc = rt.exec("hostname");
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (proc == null) {
                UIController.println("Could not get hostname!");
                throw new RuntimeException("Could not get hostname!");
            }
            BufferedReader stdInput = new BufferedReader(new
                    InputStreamReader(proc.getInputStream()));
            StringBuilder hostname = new StringBuilder();
            int nextChar;
            try {
                while ((nextChar = stdInput.read()) != -1) {
                    hostname.append((char) nextChar);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            executeRequestMap.put("sutHostname", hostname.toString().replaceAll("\\r\\n", "").toUpperCase(Locale.US));
            Request testRequest = new GetRequest("api/execute.php", executeRequestMap);
            Web.getInstance().queueRequest(testRequest);
            testRequest.waitForOutput();
            TestResponseModel model = new Gson().fromJson(testRequest.getOutput(), TestResponseModel.class);
            if (!testSuite.hasAllModels(model.getData())) {
                UIController.println("TestSuite Changes detected! Modifying TestSuite and scheduling tests. . .");
                Set<TestSuiteTestModel> testModels = testSuite.addAndGetNewModels(model.getData());
                testModels.parallelStream().forEach(testModel -> {
                    UIController.println("Processing Test Model: " + testModel);
                    HashMap<String, String> requestMap = new HashMap<>();
                    requestMap.put("testScript", testModel.getTest().getTestScript());
                    Request fileRequest = new FileRequest("api/test.php", requestMap, testModel.getTest().getTestScript());
                    Web.getInstance().queueRequest(fileRequest);
                    fileRequest.waitForOutput();
                    TestExecutor.getInstance().schedule(testModel);
                });
            }
            //end
            try {
                Thread.sleep(Web.webPollRate * 10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
