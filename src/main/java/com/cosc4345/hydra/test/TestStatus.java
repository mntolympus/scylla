/*
 * Copyright (c) 2017. Challstrom. All Rights Reserved.
 */

package com.cosc4345.hydra.test;

public enum TestStatus {
    SUCCESS, FAIL, INPROGRESS, NOTRUN
}
