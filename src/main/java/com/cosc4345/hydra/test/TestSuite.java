package com.cosc4345.hydra.test;

import com.cosc4345.hydra.model.TestSuiteTestModel;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

class TestSuite {
    private final Set<TestSuiteTestModel> models = Collections.newSetFromMap(new ConcurrentHashMap<TestSuiteTestModel, Boolean>());

    public boolean hasAllModels(Collection<TestSuiteTestModel> models) {
        return this.models.containsAll(models);
    }

    public Set<TestSuiteTestModel> addAndGetNewModels(Collection<TestSuiteTestModel> models) {
        HashSet<TestSuiteTestModel> copySet = new HashSet<>(models);
        copySet.removeAll(this.models);
        this.models.clear();
        this.models.addAll(models);
        return copySet;
    }
}
