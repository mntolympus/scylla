/*
 * Copyright (c) 2017. Challstrom. All Rights Reserved.
 */

package com.cosc4345.hydra.test;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

class TestThreadFactory implements ThreadFactory {
    private final AtomicInteger count = new AtomicInteger();
    private final ThreadGroup threadGroup = new ThreadGroup("Test Threads");

    @Override
    public Thread newThread(Runnable r) {
        Thread thread = new Thread(threadGroup, r, "TestThread-" + count.getAndIncrement());
        thread.setDaemon(false);
        thread.setPriority(Thread.MIN_PRIORITY);
        return thread;
    }
}
