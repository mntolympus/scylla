package com.cosc4345.hydra.test;

import com.cosc4345.hydra.Main;
import com.cosc4345.hydra.Web;
import com.cosc4345.hydra.model.ExecutionResponseModel;
import com.cosc4345.hydra.model.PythonResponseModel;
import com.cosc4345.hydra.model.TestModel;
import com.cosc4345.hydra.request.PostRequest;
import com.cosc4345.hydra.request.PutRequest;
import com.cosc4345.hydra.view.UIController;
import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Objects;

public class RunnableTest implements Runnable {
    private final int testId;
    private final String subId;
    private final String testName;
    private final String testScript;
    private final boolean test_isSerial;
    private TestStatus status;
    private int executionId;

    RunnableTest(TestModel testModel, String subId) {
        testId = testModel.getTestId();
        this.subId = subId;
        testName = testModel.getTestName();
        testScript = testModel.getTestScript();
        test_isSerial = testModel.isTestIsSerial();
        status = TestStatus.NOTRUN;
    }

    public int getTestId() {
        return testId;
    }

    public String getTestName() {
        return testName;
    }

    public String getTestScript() {
        return testScript;
    }

    public boolean isTest_isSerial() {
        return test_isSerial;
    }

    public TestStatus getStatus() {
        return status;
    }

    @Override
    public void run() {
        status = TestStatus.INPROGRESS;
        UIController.showTest(this);
        UIController.println("Running test number " + subId + "!");
        String prefix = "python";
        String directory = "test/";
        //request an execution id
        HashMap<String, String> executeRequestMap = new HashMap<>();
        executeRequestMap.put("sutHostname", Main.hostname);
        executeRequestMap.put("testId", String.valueOf(testId));
        PostRequest executionRequest = new PostRequest("api/execute.php", executeRequestMap);
        Web.getInstance().queueRequest(executionRequest);
        executionRequest.waitForOutput();
        //This isn't jenky at all TODO: unfrack GSON
        Gson gson = new Gson();
        ExecutionResponseModel responseModel = gson.fromJson(executionRequest.getOutput(), ExecutionResponseModel.class);
        executionId = responseModel.getData().get(0);
        String command = null;
        status = TestStatus.INPROGRESS;
        File testScriptFile = new File(directory + testScript);
        try {
            command = prefix + " " + testScriptFile.getCanonicalPath();
        } catch (IOException e) {
            e.printStackTrace();
            status = TestStatus.FAIL;
        }
        Runtime rt = Runtime.getRuntime();
        Process proc = null;
        try {
            proc = rt.exec(command);
        } catch (IOException e) {
            e.printStackTrace();
            status = TestStatus.FAIL;
        }

        if (proc == null) {
            status = TestStatus.FAIL;
            throw new RuntimeException("Could not start process for test " + testName);
        }
        BufferedReader stdInput = new BufferedReader(new
                InputStreamReader(proc.getInputStream()));

        BufferedReader stdError = new BufferedReader(new
                InputStreamReader(proc.getErrorStream()));

// read the output from the command
        UIController.println("STDOUT of " + testName + ":\n");
        StringBuilder stdoutString = new StringBuilder();
        String s;
        try {
            while ((s = stdInput.readLine()) != null) {
                UIController.println(s);
                stdoutString.append(s);
            }
        } catch (IOException e) {
            e.printStackTrace();
            status = TestStatus.FAIL;
        }

// read any errors from the attempted command
        UIController.println("STDERR of " + testName + ":\n");
        StringBuilder stderrString = new StringBuilder();
        try {
            while ((s = stdError.readLine()) != null) {
                UIController.println(s);
                stderrString.append(s);
                status = TestStatus.FAIL;
            }
        } catch (IOException e) {
            e.printStackTrace();
            status = TestStatus.FAIL;
        }
        status = status != TestStatus.FAIL ? TestStatus.SUCCESS : status;
        HashMap<String, String> statusRequestMap = new HashMap<>();
        statusRequestMap.put("executionId", "" + executionId);

        Gson gson1 = new Gson();
        PythonResponseModel pythonResponseModel = gson1.fromJson(stdoutString.toString(), PythonResponseModel.class);
        StringBuilder messagesOutput = new StringBuilder();
        if (pythonResponseModel != null) {
            for (String message :
                    pythonResponseModel.getMessages()) {
                messagesOutput.append(message);
            }
        } else {
            pythonResponseModel = new PythonResponseModel();
        }
        //FIXME: Success/failure being overwritten. Issue #2
        status = Objects.equals(pythonResponseModel.getTestStatus(), "SUCCESS") ? TestStatus.SUCCESS : TestStatus.FAIL;
        UIController.showTest(this);
        statusRequestMap.put("success", status == TestStatus.SUCCESS ? "1" : "0");
        String output = "[STDOUT]" + messagesOutput.toString() + (stderrString.length() > 0 ? "[STDERR]" + stderrString : "");
        statusRequestMap.put("output", output);
        PutRequest statusRequest = new PutRequest("api/execute.php", statusRequestMap);
        HashMap<String, String> logMap = new HashMap<>();
        logMap.put("executionId", "" + executionId);
        logMap.put("log", output);
        PostRequest logRequest = new PostRequest("api/log.php", logMap);
        Web.getInstance().queueRequest(statusRequest);
        Web.getInstance().queueRequest(logRequest);
        TestExecutor.getInstance().serialTestsRunning.getAndDecrement();
    }

    @Override
    public String toString() {
        return "RunnableTest{" +
                "testId=" + testId +
                ", subId=" + subId +
                ", testName='" + testName + '\'' +
                ", testScript='" + testScript + '\'' +
                ", test_isSerial=" + test_isSerial +
                ", status=" + status +
                ", executionId=" + executionId +
                '}';
    }
}