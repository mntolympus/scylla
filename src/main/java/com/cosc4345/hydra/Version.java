package com.cosc4345.hydra;

final class Version {
    private static final String VERSION_MAJOR = "0";
    private static final String VERSION_MINOR = "5";
    private static final String VERSION_PATCH = "1";

    static String getVersionString() {
        return VERSION_MAJOR + "." + VERSION_MINOR + "." + VERSION_PATCH;
    }
}
