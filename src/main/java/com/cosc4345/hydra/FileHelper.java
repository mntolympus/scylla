/*
 * Copyright (c) 2017. Challstrom. All Rights Reserved.
 */

package com.cosc4345.hydra;

import java.io.*;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class FileHelper {
    public static File getJarDir() {
        File jar = getJar();
        if (!jar.exists()) {
            System.err.println("Couldn't get jar at " + jar.toString());
            return null;
        }
        return jar.getParentFile();
    }

    public static File getJar() {
        String path = Main.class.getResource("").getPath();
        //cleanup the path because of java's URI/URL NONSENSE
        path = path.substring(path.indexOf(':') + 1, path.contains("!") ? path.indexOf('!') : path.length());
        return new File(path);
    }

    @Deprecated
    public static void writeStreamToFile(String path, BufferedReader reader) {
        File file = new File(path);
        FileOutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(file);
            int fileChar = 0;
            while ((fileChar = reader.read()) != -1) {
                outputStream.write(fileChar);
            }
            reader.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeStringToFile(String path, String data) {
        //TODO: Refactor to handle path names
        File file = new File("test/" + path);
        FileOutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(file);
            for (char c :
                    data.toCharArray()) {
                outputStream.write(c);
            }
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String getSelfMD5() {
        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(FileHelper.getJar());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        byte[] dataBytes = new byte[1024];

        int nread;
        try {
            if (fis != null) {
                while ((nread = fis.read(dataBytes)) != -1) {
                    if (md != null) {
                        md.update(dataBytes, 0, nread);
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] mdBytes = new byte[0];
        if (md != null) {
            mdBytes = md.digest();
        }

        //convert the byte to hex format method 1
        StringBuilder sb = new StringBuilder();
        for (byte mdByte : mdBytes) {
            sb.append(Integer.toString((mdByte & 0xff) + 0x100, 16).substring(1));
        }

        return sb.toString();
    }

}