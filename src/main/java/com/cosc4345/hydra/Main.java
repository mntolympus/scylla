/*
 * Copyright (c) 2017. Challstrom. All Rights Reserved.
 */

package com.cosc4345.hydra;

import com.cosc4345.hydra.request.GetRequest;
import com.cosc4345.hydra.test.TestExecutor;
import com.cosc4345.hydra.test.TestScheduler;
import com.cosc4345.hydra.view.UIController;
import com.cosc4345.hydra.view.Notification;

import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.util.*;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

public class Main {
    public static final int waitPollRate = 1000;
    public static final String hostname = getHostname();

    public static volatile boolean systemIsRunning = true;
    private static ExecutorService backgroundExecutor;

    public static void main(String[] args) {
        String md5 = FileHelper.getSelfMD5();
        //verify controller
        UIController.println("Current Version Hash: " + md5);
        //init
        Init.initDirectories();
        List<String> argsList = Arrays.asList(args);
        boolean useConsole = !argsList.contains("headless");
        boolean useGUI = !argsList.contains("nogui");
        boolean timed = argsList.contains("timed");

        //nThreads MUST EQUAL #ofBackgroundThreads
        backgroundExecutor = Executors.newFixedThreadPool(5);

        //Setup background objects
        UIController uiController = new UIController(useConsole, useGUI);
        Web web = Web.getInstance();
        TestScheduler testScheduler = new TestScheduler();

        //submit background tasks
        backgroundExecutor.submit(uiController);
        backgroundExecutor.submit(web);
        backgroundExecutor.submit(testScheduler);
        backgroundExecutor.submit(TestExecutor.getInstance());
        backgroundExecutor.submit(Log.getInstance());

        UIController.println("=====HYDRA Control Active=====");

        try {
            Notification.sendNotification("Startup", "Scylla controller is now Active.");
        } catch (AWTException | MalformedURLException e) {
            e.printStackTrace();
        }

        //check local version versus remote version
        HashMap<String, String> versionRequestMap = new HashMap<>();
        versionRequestMap.put("version", Version.getVersionString());
        GetRequest versionRequest = new GetRequest("dl/version.php", versionRequestMap);
        Web.getInstance().queueRequest(versionRequest);
        versionRequest.waitForOutput();
        String remoteMd5 = versionRequest.getOutput();
        if (!Objects.equals(remoteMd5, md5)) {
            UIController.println("VERSION WARNING! Remote jar version is "+remoteMd5+" but local is "+md5+"! Consider updating!");
            System.err.println("VERSION WARNING! Remote jar version is "+remoteMd5+" but local is "+md5+"! Consider updating!");
            Toolkit.getDefaultToolkit().beep();
            try {
                Notification.sendNotification("Warning!", "Scylla Controller is Out of date! Consider updating!");
            } catch (AWTException | MalformedURLException e) {
                e.printStackTrace();
            }
        }

        if (timed) {
            for (int i = 0; systemIsRunning && i < 30; i++) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            shutdownController();
        }

    }

    private static String getHostname() {
        Runtime rt = Runtime.getRuntime();
        Process proc = null;
        try {
            proc = rt.exec("hostname");
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (proc == null) {
            UIController.println("Could not get hostname!");
            throw new RuntimeException("Could not get hostname!");
        }
        BufferedReader stdInput = new BufferedReader(new
                InputStreamReader(proc.getInputStream()));
        StringBuilder hostname = new StringBuilder();
        int nextChar;
        try {
            while ((nextChar = stdInput.read()) != -1) {
                hostname.append((char) nextChar);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return hostname.toString().toUpperCase(Locale.US).replaceAll("[\\t\\n\\r]+", "").trim();
    }

    public static void shutdownController() {
        UIController.println("==Requesting HYDRA Control Shutdown==");
        //just to make sure we notify of shutdown
        System.out.println("==Requesting HYDRA Control Shutdown==");
        try {
            Notification.sendNotification("Shutdown", "Scylla controller is shutting down.");
        } catch (AWTException | MalformedURLException e) {
            e.printStackTrace();
        }
        systemIsRunning = false;
        TestExecutor.getInstance().shutdown();
        backgroundExecutor.shutdown();
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.exit(0);
    }
}
