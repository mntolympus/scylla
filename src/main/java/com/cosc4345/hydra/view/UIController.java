package com.cosc4345.hydra.view;

import com.cosc4345.hydra.Log;
import com.cosc4345.hydra.Main;
import com.cosc4345.hydra.test.RunnableTest;

import java.awt.*;
import java.util.Arrays;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class UIController implements Runnable {
    private static final ConcurrentLinkedQueue<String> stdoutQueue = new ConcurrentLinkedQueue<>();
    private static final ConcurrentLinkedQueue<RunnableTest> testQueue = new ConcurrentLinkedQueue<>();
    private final AtomicInteger lineCounter;
    private final boolean useConsole;
    private final boolean useGUI;
    private View view;
    private int maxLines;


    public UIController(boolean useConsole, boolean useGUI) {
        lineCounter = new AtomicInteger(0);
        this.useConsole = useConsole;
        this.useGUI = !GraphicsEnvironment.isHeadless() && useGUI;
        //setup gui stuff if not isHeadless
        if (useGUI) {
            //setup GUI
            UIController.println("Not in headless mode! Constructing additional pylons.");
            view = new View();
            maxLines = (int) (view.stdout.getHeight() / (1.5 * view.stdout.getFont().getSize()));
        }
        if (useConsole) {
            UIController.println("Console available! Hello World!");
        }
    }

    public static void println(String output) {
        stdoutQueue.add(output + "\n");
    }

    public static void print(String output) {
        stdoutQueue.add(output);
    }

    public static void showTest(RunnableTest test) {
        testQueue.add(test);
    }

    @Override
    public void run() {
        if (useGUI) view.setVisible(true);
        boolean changed = false;
        while (Main.systemIsRunning) {
            if (stdoutQueue.size() > 0) {
                printToAll(stdoutQueue.remove());
                changed = true;
            }
            if (useGUI) {
                if (testQueue.size() > 0) {
                    view.addExecutorLine(testQueue.remove().toString());
                    changed = true;
                }
                if (view.stdout.getLineCount() > maxLines) {
                    view.stdout.setText("");
                    changed = true;
                }
                //-5 b/c this usually wraps for no reason
                if (view.executor.getLineCount() > maxLines - 5) {
                    view.executor.setText("");
                    changed = true;
                }
            }
            if (!changed) {
                try {
                    Thread.sleep(Main.waitPollRate);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            changed = false;
        }
        closeView();
    }

    private void printToAll(String output) {
        Log.getInstance().log(output);
        output = lineCounter.getAndIncrement() + ": " + output;
        if (useConsole) {
            System.out.print(output);
        }
        if (useGUI) {
            view.stdout(output);
        }
    }

    private void closeView() {
        if (useGUI) {
            view.dispose();
            view.removeListeners();
            Arrays.stream(Window.getWindows()).forEach(Window::dispose);
        }
    }
}


