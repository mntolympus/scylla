package com.cosc4345.hydra.view;

import com.cosc4345.hydra.Main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

class View extends JFrame {
    final JTextArea stdout;
    final JTextArea executor;
    private final JButton stopButton;
    private final ActionListener stopListener;

    View() {
        super("Hydra Controller");
        stopListener = (event) -> Main.shutdownController();
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        int screenHeight = (int) screenSize.getHeight();
        int screenWidth = (int) screenSize.getWidth();

        int frameHeight = screenHeight / 2;
        Dimension compDim = new Dimension(screenWidth / 2, frameHeight / 2);


        //Setup this
        JFrame.setDefaultLookAndFeelDecorated(true);

        //Init items
        stdout = new JTextArea();
        executor = new JTextArea();
        stopButton = new JButton("Stop Hydra Controller");
        JPanel wrapper = new JPanel();

        //Configure items and this
        stdout.setEditable(false);
        stdout.setMaximumSize(compDim);
        stdout.setFont(stdout.getFont().deriveFont(10f));
        stdout.setLineWrap(true);

        executor.setEditable(false);
        executor.setMaximumSize(compDim);
        executor.setFont(executor.getFont().deriveFont(10f));
        executor.setLineWrap(true);

        stopButton.setSize(screenWidth / 2, frameHeight / 3);
        stopButton.setFont(executor.getFont().deriveFont(10f));

        this.setMinimumSize(new Dimension(screenWidth, frameHeight));
        this.setLayout(new BorderLayout());
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        //Listeners
        stopButton.addActionListener(stopListener);

        wrapper.setLayout(new BoxLayout(wrapper, BoxLayout.X_AXIS));

        //Add items to this and wrapper
        wrapper.add(stdout);
        wrapper.add(executor);

        this.add(wrapper, BorderLayout.CENTER);
        this.add(stopButton, BorderLayout.SOUTH);

        this.pack();
    }

    void stdout(String line) {
        stdout.setText(stdout.getText() + line);
    }

    void addExecutorLine(String line) {
        executor.setText(executor.getText() + line + '\n');
    }

    void removeListeners() {
        stopButton.removeActionListener(stopListener);
    }
}