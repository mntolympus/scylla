package com.cosc4345.hydra.view;

import java.awt.*;
import java.awt.TrayIcon.MessageType;
import java.io.IOException;

public class Notification {
    public static void sendNotification(String title, String message) throws AWTException, java.net.MalformedURLException {
        if (SystemTray.isSupported()) {
            //Obtain only one instance of the SystemTray object
            SystemTray tray = SystemTray.getSystemTray();

            //If the icon is a file
            Image image = Toolkit.getDefaultToolkit().createImage("icon.png");

            //Alternative (if the icon is on the classpath):
//        Image image = Toolkit.getDefaultToolkit().createImage(Notification.class.getResource("icon.png"));
            TrayIcon trayIcon = new TrayIcon(image, "Scylla Notification");
            trayIcon.setImageAutoSize(true);
            //Let the system resizes the image if needed
            trayIcon.setImageAutoSize(true);
            //Set tooltip text for the tray icon
            trayIcon.setToolTip("Scylla Notification");
            tray.add(trayIcon);
            trayIcon.displayMessage(title, message, MessageType.INFO);
        }
        if (System.getProperty("os.name").toLowerCase().startsWith("mac os x")) {
            //we're running Mac!
            try {
                Runtime.getRuntime().exec("osascript -e 'display notification \""+message+"\" with title \""+title+"\"'");
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }
}
