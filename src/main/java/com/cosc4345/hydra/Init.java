/*
 * Copyright (c) 2017. Challstrom. All Rights Reserved.
 */

package com.cosc4345.hydra;

import java.io.File;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

//TODO: should we do dynamic init?
class Init {
    private static final HashSet<File> expectedDirectories = new HashSet<>();

// --Commented out by Inspection START (07-Nov-17 01:47 PM):
//    static void initAll() {
//        initDirectories();
//    }
// --Commented out by Inspection STOP (07-Nov-17 01:47 PM)

// --Commented out by Inspection START (07-Nov-17 01:47 PM):
//    static boolean checkAll() {
//        return checkDirectories();
//    }
// --Commented out by Inspection STOP (07-Nov-17 01:47 PM)

    static void initDirectories() {
        //TODO: replace with parts
        File jar = FileHelper.getJar();
        File jarDir = FileHelper.getJarDir();
        expectedDirectories.add(new File(jarDir + "/test"));
        expectedDirectories.add(jar);

        assert jarDir != null;
        List<File> currentFiles = Arrays.asList(jarDir.listFiles());

        if (!currentFiles.containsAll(expectedDirectories)) {
            //we're missing expected directories!
            expectedDirectories.removeAll(currentFiles);
            int dirsCreated = 0;
            for (File file : expectedDirectories) {
                dirsCreated = file.mkdir() ? 1 : 0;
            }

            //TODO: update number of dirs created
            if (dirsCreated < 1) {
                System.err.println("Could not create all directories!");
                System.exit(-1);
            }
        }
    }

    private static boolean checkDirectories() {

        //TODO: replace with parts
        File[] files = {new File("./test/directorList.py")};
        boolean allFound = true;
        for (File file :
                files) {
            if (!file.exists()) allFound = false;
        }
        return allFound;
    }
}
