package com.cosc4345.hydra.testutils;

/**
 * Created by TJ Challstrom on 01-Oct-17 at 03:57 PM.
 * Dispenser goes here!
 */
public class EO<T> {
    public String getEO(T expected, T observed) {
        return "\tExpected: " + expected + "! Observed: " + observed + "!";
    }
}
